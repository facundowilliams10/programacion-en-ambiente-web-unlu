class PawMenu {
	constructor(pContenedor) {
		
		let pantallaMobile = window.matchMedia("(max-width: 768px)"); //para consultar resolucion de pantalla

		//Se consigue el nodo contenedor
		let contenedor = pContenedor.tagName
			? pContenedor
			: document.querySelector(pContenedor);

		if ( (contenedor) && ( pantallaMobile.matches ) )
		{
            //solo si se consigue el nodo contenedor Y la resolucion de pantalla "es mobile"

            //se agregan las clases
			contenedor.classList.add("PawMenu");
			contenedor.classList.add("PawMenuCerrado");
            
			//Se inserta elementos multi-nivel, sin necesidad de modificar el backend

			let flechaAbajo = Paw.nuevoElemento("span", "", {class: "icono-flecha-abajo-blanca"});

			//TO-DO hacer "turnos" creando un nuevo li
			Paw.insertarUl(".PawMenu ul li:nth-child(1)", {class: "ul-turnos"}); //TO-DO hacerlo mas escalable
			Paw.insertarLi("Nuevo", ".ul-turnos", {class: "li-nuevo-turno", href: "/nuevo-turno"});
			Paw.insertarLi("Listado", ".ul-turnos", {class: "li-listado-turnos", href: "/listado-turnos"});
            

			
			//se linkea el estilo
			let css = Paw.nuevoElemento("link", "", {
				rel: "stylesheet",
				href: "js/components/styles/PawMenu.css",
			});
			document.head.appendChild(css);

			//Se arma el boton

			let boton = Paw.nuevoElemento("button", "", {class: "PawMenuAbrir",});

            //se da el comportamiento al boton dependiendo de si el nodo sobre el cual se hizo click estaba en "modo abierto o modo cerrado"
			boton.addEventListener("click", (event) => {
				if (event.target.classList.contains("PawMenuAbrir")) {
					event.target.classList.add("PawMenuCerrar");
					event.target.classList.remove("PawMenuAbrir");
					contenedor.classList.add("PawMenuAbierto");
					contenedor.classList.remove("PawMenuCerrado");
				} else {
					event.target.classList.add("PawMenuAbrir");
					event.target.classList.remove("PawMenuCerrar");
					contenedor.classList.add("PawMenuCerrado");
					contenedor.classList.remove("PawMenuAbierto");
				}
			});

			// Se inserta el boron en el contenedor
			contenedor.prepend(boton);
			
			//TO-DO hacer "turnos" creando un nuevo li
			let menuTurnos = document.querySelector(".PawMenu > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)");
			menuTurnos.innerHTML = "Turnos";
			menuTurnos.after(flechaAbajo);
			menuTurnos.removeAttribute("href");
			menuTurnos.addEventListener("click", () =>{
				let submenuTurnos = document.querySelector(".ul-turnos");

				if (submenuTurnos.classList.contains("mostrar-submenu")) {
					submenuTurnos.classList.remove("mostrar-submenu");
				} else {
					submenuTurnos.classList.add("mostrar-submenu");
				}	
			}
			);

			flechaAbajo.addEventListener("click", () =>{
				let submenuTurnos = document.querySelector(".ul-turnos");

				if (submenuTurnos.classList.contains("mostrar-submenu")) {
					submenuTurnos.classList.remove("mostrar-submenu");
				} else {
					submenuTurnos.classList.add("mostrar-submenu");
				}	
			}
			);

		} else {
			if (!pantallaMobile.matches)
				console.info("La pantalla no es mobile, no se debe mostrar el menu hamburguesa");
			else
				console.error("Elemento HTML para generar el MENU no encontrado");
		}
	}
	

		/*Intento de hacerlo sin variables, no funciona
		-Pablo: Ojo. El appendChild retorna el elemento hijo, no al que le agregaste el elemento, 
		por eso se desaconseja hacer estos encadenamientos. El codigo que escribis no hace lo que vos querés, 
		pero al retornar el hijo, agregas al li el texto, y eso retorna el texto, por lo cual agregas a lista el texto.*/
		// document.querySelector(lista).appendChild(
		// 	document.createElement('li').appendChild(
		// 		document.createElement('a').appendChild(
		// 			document.createTextNode("Hola Insertar 2")
		// 		)
		// 	)
		// );

	
}