class PawAgenda {
    constructor(pContenedor) {
        
        let formulario = pContenedor.tagName ? pContenedor : document.querySelector(pContenedor);
        let profesional = formulario.querySelector("#profesional");
        let especialidad = formulario.querySelector("#especialidad");
        let inp_fecha = formulario.querySelector("#FechaTurno");
        let inp_hora = formulario.querySelector("#HoraTurno");

        // Esconder fecha y hora (usando los padres que son labels).
        inp_fecha.closest('label').style.display = 'none';
        inp_hora.closest('label').style.display = 'none';
       
        profesional.addEventListener("change", ()=>{
            if ( '' !==profesional.value ) {
                console.log(profesional.value);

                // Enviamos los datos al backend y recibimos los datos en formato json
                fetch("/ObtenerHorarios?id_prof=1&id_esp=1")
                    .then(response => response.json())
                    .then( data => { 
                          this.tableCreate(data,formulario);
                    })
                    .catch( e => console.error('Error: ' + e ))
            }else{
                console.log("No se eleigio nada");
            }
        });

        // Click al seleccionar un Horario Disponible.

    }

    mostrarAlarma( td, parent, tb ){
        let horario_disp = td.querySelector('.horario').value.split('@') ;
        let inp_fecha = parent.querySelector("#FechaTurno");
        let inp_hora  = parent.querySelector("#HoraTurno");
        inp_fecha.closest('label').style.display = 'block';
        inp_hora.closest('label').style.display = 'block';
        inp_fecha.value = horario_disp[1].toString();
        inp_hora.value = horario_disp[0].toString();
        tb.remove();
        console.log('hora set: '+horario_disp[1].toString() + '--->' + inp_hora.value );
        console.log('fecha set: '+horario_disp[0].toString() + '--->' + inp_fecha.value );
    }


    tableCreate( data, parent ){
        // Creo un array de horas para luego ingresarlo en cada entrada de la tabla.
        let horas = this.createArrayHoras(data.horarioInicio,data.horarioFinalizacion);

        let dias_y_turnos = [];
        
        // Agrupo en array los dias y turnos tomados.
        dias_y_turnos= this.columns_dias(data.turnosTomados, data.diasQueAtiende);
        // Creo la Tabla.
        let tableHorarios = document.createElement('table');
        tableHorarios.style.width = '100%';
        tableHorarios.setAttribute('border', 'bold');
        var tbdy = document.createElement('tbody');

        // Fila de theads.
        var tr = Paw.nuevoElemento("tr","");
        // Se crean las columnas.
        tr.appendChild(Paw.nuevoElemento("th","Hora"));

        for (var key in dias_y_turnos) {
            let key_arr = key.split('@');
            let fecha   = key_arr[0].split('-');
            tr.appendChild(Paw.nuevoElemento("th",key_arr[1]+' ('+fecha[2]+'/'+fecha[1]+')'));
        }
        // Inserto filas de heads.
        tableHorarios.appendChild(tr);

        // Cuerpo de la tabla.
        horas.forEach(hora => {
            let tr = Paw.nuevoElemento("tr","");
            tr.appendChild(Paw.nuevoElemento("td",hora));
            // Recorro el array con keys cada elemento es e.g: lunes@14-06-2021.
            for (var key in dias_y_turnos) {
                let estado  = 'Disponible';
                if(  4 < Object.keys(dias_y_turnos).length ){
                    estado  = 'Disp.';
                }
                let clase   = 'disponible';
                dias_y_turnos[key].forEach(dia_oc => {
                    let hora_dia = JSON.stringify(dia_oc.horas);
                    let min_dia  = JSON.stringify(dia_oc.minutos);
                    if ( hora_dia.padStart(2,'0')+':'+ min_dia.padStart(2,'0') === hora) {
                        estado = 'No disponible'
                        if(  4 < Object.keys(dias_y_turnos).length ){
                            estado  = 'No Disp.';
                        }
                        clase  = 'no-disponible';
                    }
                });
                let td =Paw.nuevoElemento("td",estado,{class: clase});
                
                if ( 'disponible' === clase ) {
                    let key_arr = key.split('@');
                    let horario = key_arr[0] + '@' +hora;
                    td.appendChild(Paw.nuevoElemento("input","",{class:'horario', type:'hidden', value:horario}));        
                    td.addEventListener("click",()=>{
                        this.mostrarAlarma( td, parent, tableHorarios);
                    } );
                }
                tr.appendChild(td);
            }
            tbdy.appendChild(tr);
        });


        // Insertando cuerpo de la tabla.
        tableHorarios.appendChild(tbdy);
        parent.insertBefore( tableHorarios, parent.children[7]);
    };

    createArrayHoras(horaInicio, horaFin) {
        let array_horas = [];

        for (let index = horaInicio.horas; index <= horaFin.horas; index++){
            array_horas.push(index +':00');
            if (index !== horaFin.horas )
                array_horas.push(index +':30');
        }
        return array_horas;
    };

    asignarDia( dia_semana){
        let dia = '';
        switch (dia_semana){
            case '1': dia = 'Lunes';
                    break;
            case '2': dia = 'Martes';
                    break;
            case '3': dia = 'Miercoles';
                    break;
            case '4': dia = 'Jueves';
                    break;
            case '5': dia = 'Viernes';
                    break;
            case '6': dia = 'Sabado';
                    break;
            case '0': dia = 'Domingo';
                    break;
            case 'Lunes': dia = 1;
                    break;
            case 'Martes': dia = 2;
                    break;
            case 'Miercoles': dia = 3;
                    break;
            case 'Jueves': dia = 4;
                    break;
            case 'Viernes': dia = 5;
                    break;
            case 'Sabado': dia = 6;
                    break;
            case 'Domingo': dia = 0;
                    break;
        };
        return dia;
    }

    columns_dias( turnos_tomados,diasQueAtiende){
        let today = new Date();//Obtengo fecha de hoy.
        let array_dias=[]; 
        // Array con dias para las columnas de la tabla.
        for (let index = 0; index < 8; index++) {
            // Preparo numero de la semana de hoy.
            let n_day = today.getDay();
            n_day = this.asignarDia(n_day.toString());
            // Verifico si existe en los dias que atiende.
            if ( diasQueAtiende.indexOf(n_day) !== -1 ){
                // Preparo mes de hoy.
                let month = today.getMonth()+1;
                month = month.toString();
                // preparando string de fecha.
                let fecha_str = today.getFullYear()+'-'+month.padStart(2,'0')+'-'+today.getDate().toString().padStart(2,'0');
                // creo array con el dia que atiende.
                array_dias[fecha_str+'@'+n_day]=[];
                // Recorro Turnos para crear array con sus respectivos turnos.
                turnos_tomados.forEach( turno => {
                    let  fecha_turno_arr = turno.fecha.split('-');
                    let fecha = new Date(fecha_turno_arr[0]+'-'+fecha_turno_arr[1].padStart(2,'0')+'-'+(parseInt(fecha_turno_arr[2])+1).toString().padStart(2,'0') );
                    // Agregando al array fechas de una semana en adelante.
                    if (turno.fecha === fecha_str){
                        array_dias[fecha_str+'@'+n_day].push(turno);
                    }
                });  
            }
            today.setDate(today.getDate()+1);
        }
        return array_dias;
    }
}
