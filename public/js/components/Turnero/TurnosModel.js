class TurnosModel {
    constructor() {

      //Para recuperar los turnos desde el Local Storage (persistencia ante el refresh)  o un array vacio
      this.turnos = JSON.parse(localStorage.getItem('turnos')) || [];
          
      // El estado del Modelo, un array de objetos Turno, pre-cargado con la info del JSon de ejemplo
      this.turnos = [
        {
            id: 1,
            dia: "Lunes",
            horas: 9,
            minutos: 0,
            nombreProfesional: "Tekito",
            apellidoProfesional: "Lakarie",
            nombrePaciente: "Juan",
            apellidoPaciente: "Perez",
            atendiendo: false
        },
        {
            id:2,
            dia: "Martes",
            horas: 9,
            minutos: 0,
            nombreProfesional: "Tekito",
            apellidoProfesional: "Lamuela",
            nombrePaciente: "Juana",
            apellidoPaciente: "Perez",
            atendiendo: true
        }
    ]
}  

    addTurno(id, dia, horas, minutos, nombreProfesional, apellidoProfesional, nombrePaciente, apellidoPaciente) {
      
      const turno = {
        //Se crea un id univoco para el turno
        id: this.turnos.length > 0 ? this.turnos[this.todos.length - 1].id + 1 : 1,
        dia : dia,
        horas: horas,
        minutos: minutos,
        nombreProfesional: nombreProfesional,
        apellidoProfesional: apellidoProfesional,
        nombrePaciente: nombrePaciente,
        apellidoPaciente: apellidoPaciente
      }
      this.turnos.push(turno);
      this.onTurnoListChanged(this.turnos);
  }

  deleteTurno(id){
      this.turnos = this.turnos.filter((turno) => turno.id !== id );
  }

  //La siguiente funcion es para cambiar el estado del turno entre "atendiendo" y "no atendiendo"
  toggleTurno(id){
      this.turnos = this.turnos.map((turno) =>
      (turno.id === id) ? 
        {dia: turno.dia, horas: turno.horas, minutos: turno.minutos, nombreProfesional: turno.nombreProfesional, apellidoProfesional: turno.apellidoProfesional,
        nombrePaciente: turno.nombrePaciente, apellidoPaciente: turno.apellidoPaciente, atendiendo: !turnos.atendiendo} : turno,
      );
  }

    //Con bindTurnosChanged el modelo avisa al controlador que se producieron cambios
  bindTurnosChanged(callback) {
    this.onTurnosListChanged = callback;
  }

  //metodo privado para guardar los datos de los turnos en el Local Storage
  _commit(turnos) {
    this.onTurnosListChanged(turnos);//avisa que hay cambios en el modelo de datos
    localStorage.setItem('turnos', JSON.stringify(turnos));
  }

}