class TurneroController {
    constructor(model, view, pContenedor) {
      this.model = model;
      this.view = view;
      		//Se consigue el nodo contenedor
		let contenedor = pContenedor.tagName 
    ? pContenedor
    : document.querySelector(pContenedor);

  if (contenedor)
  {
    //se linkea al HTML el estilo
    let css = Paw.nuevoElemento("link", "", {
      rel: "stylesheet",
      href: "js/components/styles/Turnero.css",
    });
    document.head.appendChild(css);
    //Se crea el elemento root
    pContenedor.appendChild(
    Paw.nuevoElemento("div", "", {id: "root"}));
    //Se muestran los turnos iniciales
    this.onTurnosListChanged(this.model.turnos);

    // Binding explicito
    this.model.bindTurnosChanged(this.onTurnosListChanged);
    this.view.bindAsignarTurno(this.handleAddTurno);

  } else {
      console.error("Elemento HTML para generar el Turnero no encontrado");
    }
  }
    
  onTurnosListChanged = turnos => {
    this.view.displayTurnos(turnos)
  }
    
  //Se definen los handlers, las funciones que se deberian ejecuta cuando se disparan los eventos correspondientes

  handleAddTurno = (id) => {
    this.model.addTurno(id)
  }

  handleToggleTurno = id => {
    this.model.toggleTurno(id);
  }

}

//Usar funciones flecha nos permite utilizar el this con el contexto del controlador, cuando llamemos las funciones desde la vista:

  this.view.bindAddTurno(this.handleAddTodo)
  this.view.bindToggleTurno(this.handleToggleTodo)

    //TO-DO hacer los handlers para los requisitos:
    //   que le muestre al usuario (Actualizable cada 10 seg) que turno tiene, 
    //   cual es el que se está atendiendo, tiempo estimado de espera. 
    // Agregar funcionalidades que permitan, según el contexto, 
    // al paciente aceptar el turno y al médico terminar de atender un paciente 
    // y pasar al siguiente turno. 

  //Referencias para construir el framework: https://www.taniarascia.com/javascript-mvc-todo-app/ 