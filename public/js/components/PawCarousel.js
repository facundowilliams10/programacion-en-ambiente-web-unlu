class PawCarousel{

    
    slideAnterior = 0; // usado solo para el efecto de slide
    slideActual; // slide actual a mostrar
    slides; // conjunto de imagenes
    totalSlides; // cantidad de imagenes
    
    carouselImages; // contenedor de imagenes
    
    animacion = 'fade'; // animacion actual de transicion
    animacion_Vieja; //animacion anterior de transicion

    thumbs; //thumbs de imagenes

    labelProg;
    progressBar;
    imgDone;
    imgTotal;
    
    // pContenedor = contenedor que tendrá el carousel
    // carpetaImg = path de la carpeta que contiene las imagenes (ejemplo: /imagenes/)
    constructor(){
        //se linkea el estilo
        let css = Paw.nuevoElemento("link", "", {
            rel: "stylesheet",
            href: "js/components/styles/PawCarousel.css",
        });
        document.head.appendChild(css);

        this.slideActual = 1;
        this.slides = document.querySelectorAll('.carousel_item');
        this.totalSlides = this.slides.length;
        
        let container = document.querySelector(".carousel_container");
        this.carouselImages = document.querySelector('.carousel_images')
        

        // Crear clones de la primer y ultima slide, luego meterlos en el carrusel
        let clonPrimero = this.slides[0].cloneNode(true);
        clonPrimero.classList.add("clon_primero");
        let clonUltimo = this.slides[this.totalSlides-1].cloneNode(true);
        clonUltimo.classList.add("clon_ultimo");
        this.carouselImages.insertBefore(clonUltimo, this.carouselImages.childNodes[0]);
        this.carouselImages.appendChild(clonPrimero);
        // Incorporo los clones en la variable slides
        this.slides = document.querySelectorAll('.carousel_item');
        this.totalSlides = this.slides.length;

        
        let loader = Paw.nuevoElemento("div", "", {class: 'loader'});
        this.carouselImages.parentNode.parentNode.insertBefore(loader, this.carouselImages.parentNode);



        // ==================================================== BARRA DE PROGRESO========================================
        this.imgDone = 0;
        this.imgTotal = this.slides.length - 2;
        let barraProg = Paw.nuevoElemento("progress", "", {
            id: "idProgress",
            value: '0',
            max: '100'
        });
        this.labelProg = Paw.nuevoElemento("label", 'Imagenes cargadas: 0/' + this.imgTotal, {for: "idProgress", id: "idLabelProgress"});
        

        container.parentNode.insertBefore(this.labelProg, container);
        container.parentNode.insertBefore(barraProg, container);
        this.carouselImages.parentNode.parentNode.insertBefore(barraProg, this.carouselImages.parentNode);
        this.progressBar = document.getElementById("idProgress");
        this.progressBar.value = 0;
        

        // Evento progress para las imagenes
        for(let index = 1; index < this.totalSlides - 1; index++){
            let img =this.slides[index].querySelector(".image");
            let timer;
            // Si no esta completo
            if(!img.complete){
                var obj = this;
                img.onload = function(e){
                    timer = setInterval(
                        () => {
                            if(img.complete){
                                clearInterval(timer);
                                obj.updateBar();
                            }
                        },
                        200
                    );
                }
            }else{
                this.updateBar();
            }
        }
        // ============================================================================================================================





        // ====================Generar botones=====================
        let botonAnt = Paw.nuevoElemento("button", String.fromCharCode(10092), 
            {id: "carousel_anterior", 'aria-label': "anterior slide"});
        let botonNext = Paw.nuevoElemento("button", String.fromCharCode(10093), 
            {id: "carousel_siguiente", 'aria-label': "anterior slide"});
        let sectionButtons = Paw.nuevoElemento("section", "", {class: "carousel_buttons"});
        sectionButtons.appendChild(botonAnt);
        sectionButtons.appendChild(botonNext);
        document.querySelector(".carousel_container").appendChild(sectionButtons);

        // Da el evento click a los botones
        document.getElementById('carousel_siguiente')
            .addEventListener("click", () => {
                this.moveToNextSlide();
            });
        document.getElementById('carousel_anterior')
            .addEventListener("click", () => {
                this.moveToPrevSlide();
            });
        // ========================================================================================

        // ==================Generar Thumbs======================
        let sectionThumbs = Paw.nuevoElemento("section", "", {class: "thumbs"});
        for(let index = 1; index < this.slides.length - 1; index++){
            let linkImg = this.slides[index].querySelector('img').src;
            let img = Paw.nuevoElemento("img", "", {class: "thumb", src: linkImg});
            // Evento de click
            img.addEventListener("click", () => {
                this.moveToSlide(index);
            }); 
            sectionThumbs.appendChild(img);
        }
        let phone = document.querySelector(".container-tel");
        phone.parentNode.insertBefore(sectionThumbs, phone);
        // ========================================================================================


        // Generar Radio Buttons de animaciones
        let sectionRadio = Paw.nuevoElemento("section", "", {class: "radioButtons"});
            // Input Fade
        let inputRadioFade = Paw.nuevoElemento("input", "", 
            {type: "radio", name: "animation", id: "fade", value: "Fade Animation"}
        );
            // Label Fade
        let labelFade = Paw.nuevoElemento("label", "Fade Animation",
            {for: inputRadioFade.id, class: "label1"});
            // Input slider
        let inputRadioSlider = Paw.nuevoElemento("input", "", 
            {type: "radio", name: "animation", id: "slider", value: "Slider Animation"}
        );
            // Label slider
        let labelSlider = Paw.nuevoElemento("label", "Slider Animation",
            {for: inputRadioSlider.id, class: "label2"});
        
        sectionRadio.appendChild(inputRadioFade);
        sectionRadio.appendChild(labelFade);
        sectionRadio.appendChild(inputRadioSlider);
        sectionRadio.appendChild(labelSlider);
        phone.parentNode.insertBefore(sectionRadio, phone);



        // Pasar imagenes con las teclas
        // Por ahora, el evento lo tiene el documento, no el carousel
        document.addEventListener("keydown", () => {
            switch (event.keyCode) {
                case 37:
                    // Izquierda
                    this.moveToPrevSlide();
                    break;
                case 39:
                    // Derecha
                    this.moveToNextSlide();
                    break;
                }
        });

        // Fade radio button
        let radioFade = document.getElementById("fade");
        radioFade.addEventListener("click", () => {
            this.setearAnimacion('fade');
        });
        // Slider radio button
        let radioSlider = document.getElementById("slider");
        radioSlider.addEventListener("click", () => {
            this.setearAnimacion('slider');
        });
        
        document.querySelector(".carousel_container").addEventListener("touchstart", (event) => {
            this.touchstartX = event.changedTouches[0].screenX;
            this.touchstartY = event.changedTouches[0].screenY; 
        }, false);
        document.querySelector(".carousel_container").addEventListener('touchend',(event) => {
            this.touchendX = event.changedTouches[0].screenX;
            this.touchendY = event.changedTouches[0].screenY;
            this.handleGesure();
        }, false); 

            
        // Swipe del carousel, sin dejar hacer scroll en la ventana
        container.addEventListener( "touchstart", function ( event ) {
            let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
            let scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;
            window.onscroll = function() {
                window.scrollTo(scrollLeft,scrollTop);
            };
        } );
        container.addEventListener( "touchend", function ( event ) {
            window.onscroll = function() {};
        } );


    }// -----------------------------End Constructor---------------------------

    // Actualiza el progreso
    updateBar(){
        this.imgDone++;
        this.progressBar.value = this.imgDone / this.imgTotal * 100;
        this.labelProg.innerHTML = 'Imagenes cargadas: ' + this.imgDone + '/' + this.imgTotal;
        if(this.imgDone === this.imgTotal){
            window.setInterval(() => {
                this.moveToNextSlide();
            },5000);
            var loader = document.querySelector(".loader");
            loader.parentNode.removeChild(loader);
        }
    }

    
    updateSlidePosition(){
        // Quitar animacion vieja
        if(!(this.animacion_Vieja === this.animacion)){
            for(let slide of this.slides){
                slide.classList.remove(this.animacion_Vieja);
                slide.classList.add(this.animacion);
            }
        }
        if(this.animacion === 'fade'){
            if(this.slideActual === 0) this.slideActual = this.totalSlides - 2;
            if(this.slideActual === this.totalSlides - 1) this.slideActual = 1;
            this.slides[this.slideAnterior].classList.remove('carousel_item--visible');    
            this.slides[this.slideActual].classList.add('carousel_item--visible');    
        }else if(this.animacion === 'slider'){

            if(this.slideActual !== this.slideAnterior + 1 && this.slideActual !== this.slideAnterior - 1){
                if(this.slideActual > this.slideAnterior){
                    // Se mueve más de un slide a la vez, hacia adelante (next)
                    this.slides[this.slideAnterior + 1].classList.remove("carousel_item--visible");
                    this.slides[this.slideActual].classList.add("carousel_item--visible");
                    this.carouselImages.classList.add("classSlideTranslateNext");
                    setTimeout(() => {
                        this.slides[this.slideAnterior].classList.remove("carousel_item--visible");
                        this.slides[this.slideAnterior - 1].classList.remove("carousel_item--visible");
                        this.slides[this.slideActual + 1].classList.add("carousel_item--visible");
                        this.slides[this.slideActual - 1].classList.add("carousel_item--visible");
                        this.carouselImages.classList.remove("classSlideTranslateNext");
                    }, 200);
                }else if(this.slideActual < this.slideAnterior){
                    // Se mueve más de un slide a la vez, hacia atras (ant)
                    this.slides[this.slideAnterior - 1].classList.remove("carousel_item--visible");
                    this.slides[this.slideActual].classList.add("carousel_item--visible");
                    this.carouselImages.classList.add("classSlideTranslateAnt");
                    setTimeout(() => {
                        this.slides[this.slideAnterior].classList.remove("carousel_item--visible");
                        this.slides[this.slideAnterior + 1].classList.remove("carousel_item--visible");
                        this.slides[this.slideActual + 1].classList.add("carousel_item--visible");
                        this.slides[this.slideActual - 1].classList.add("carousel_item--visible");
                        this.carouselImages.classList.remove("classSlideTranslateAnt");
                    }, 200);
                }
            }else{
                if(this.slideActual === 0){
                    // Muestra la ultima original
                    // Mueve al anterior
                    this.carouselImages.classList.add("classSlideTranslateAnt");
                    setTimeout(() => {
                        // Oculto slides
                        this.ocultarSlides();
                        // Muestro el slide original del clon y los de al lado
                        this.slideActual = this.totalSlides - 2;
                        this.mostrarSlides();
                        this.carouselImages.classList.remove("classSlideTranslateAnt");
                    }, 200);
                }else if(this.slideActual === this.totalSlides - 1){
                    // Muestra el primero original
                    // Mueve al siguiente
                    this.carouselImages.classList.add("classSlideTranslateNext");
                    setTimeout(() => {
                        // Oculto slides
                        this.ocultarSlides();
                        // Muestro el slide original del clon y los de al lado
                        this.slideActual = 1;
                        this.mostrarSlides();
                        this.carouselImages.classList.remove("classSlideTranslateNext");
                    }, 200);
                }else{
                    // Mueve normalmente
                    if(this.slideActual > this.slideAnterior){
                        // Mueve al siguiente
                        this.carouselImages.classList.add("classSlideTranslateNext");
                        setTimeout(() => {
                            this.slides[this.slideActual - 2].classList.remove("carousel_item--visible");
                            this.slides[this.slideActual + 1].classList.add("carousel_item--visible");
                            this.carouselImages.classList.remove("classSlideTranslateNext");
                        }, 200);
                    }else{
                        // mueve al anterior
                        this.carouselImages.classList.add("classSlideTranslateAnt");
                        setTimeout(() => {
                            this.slides[this.slideActual + 2].classList.remove("carousel_item--visible");
                            this.slides[this.slideActual - 1].classList.add("carousel_item--visible");
                            this.carouselImages.classList.remove("classSlideTranslateAnt");
                        }, 200);
                    }
                }
            }
        }
    }

    moveToNextSlide(){
        this.slideAnterior = this.slideActual;
        this.slideActual++;
        this.updateSlidePosition();
    }

    moveToPrevSlide(){
        this.slideAnterior = this.slideActual;
        this.slideActual--;
        this.updateSlidePosition();
    }
    
    moveToSlide(indexSlide){
        this.slideAnterior = this.slideActual;
        this.slideActual = indexSlide;
        this.updateSlidePosition();
    }

    // Setea la animacion
    setearAnimacion(animacionNueva){
        if(animacionNueva !== this.animacion){
            this.animacion_Vieja = this.animacion; // es la actual
            this.animacion = animacionNueva; // animacion nueva
            // Quito animacion vieja (slider en este caso, del carousel)
            for(let slide of this.slides){
                slide.classList.add(this.animacion);
                slide.classList.remove(this.animacion_Vieja);
            }
            if(this.animacion === 'fade'){
                this.carouselImages.classList.remove("classSlide");
                // Oculto figures
                this.ocultarSlides();
                // Hago visible la imagen actual
                this.slides[this.slideActual].classList.add('carousel_item--visible'); 
            }else if(this.animacion === 'slider'){
                this.carouselImages.classList.add("classSlide");
                // Oculto todos los slides
                this.ocultarSlides();
                // Muestro solo el actual y los de al lado
                this.mostrarSlides();
            }
        }
    }

    handleGesure(){
        if (this.touchendX < this.touchstartX) {
            // swipea hacia la izquierda, mostrar imagen +1
            this.moveToNextSlide();
        }
        if (this.touchendX > this.touchstartX) {
            // swipea hacia la derecha, mostrar imagen -1
            this.moveToPrevSlide();
        }
    }

    ocultarSlides(){
        // Oculto todos los slides
        for(let slide of this.slides){
            slide.classList.add('carousel_item--hidden');
            slide.classList.remove('carousel_item--visible');
        }
    }

    // Mostrar slides actuales function
    mostrarSlides(){
        this.slides[this.slideActual - 1].classList.add("carousel_item--visible");
        this.slides[this.slideActual - 1].classList.remove("carousel_item--hidden");

        this.slides[this.slideActual].classList.add("carousel_item--visible");
        this.slides[this.slideActual].classList.remove("carousel_item--hidden");

        this.slides[this.slideActual + 1].classList.add("carousel_item--visible");
        this.slides[this.slideActual + 1].classList.remove("carousel_item--hidden");
    }
    
}
