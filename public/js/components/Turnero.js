
    class Turnero {
	constructor(pContenedor) {
		
		//Se consigue el nodo contenedor
		let contenedor = pContenedor.tagName //Aca voy a tener main
			? pContenedor
			: document.querySelector(pContenedor);

		if (contenedor)
		{
			//se linkea al HTML el estilo
			let css = Paw.nuevoElemento("link", "", {
				rel: "stylesheet",
				href: "js/components/styles/Turnero.css",
			});
			document.head.appendChild(css);

            //let botonTurnero = Paw.nuevoElemento("button", "Mostrar Turnero", {class:"button"});

            //contenedor.prepend(botonTurnero);

            let liTurnero = Paw.insertarLi("Turnero", "header nav ul");

            liTurnero.addEventListener("click", (event) =>{
                //Cuando hacen click en Turnero abro una nueva ventana y construyo al turnero a puro JS
                window.open("js/components/html/Turnero.html"); 
                document.querySelector("body").appendChild(
                    document.createElement("div"));
            })


		} else {
				console.error("Elemento HTML para generar el Turnero no encontrado");
		}
	}
		
}