class DragAndDrop {
	constructor(pContenedor) {
		
		//Se consigue el nodo contenedor
		let contenedor = pContenedor.tagName //Aca voy a tener el form
			? pContenedor
			: document.querySelector(pContenedor);

		if (contenedor)
		{
			//se linkea al HTML el estilo
			let css = Paw.nuevoElemento("link", "", {
				rel: "stylesheet",
				href: "js/components/styles/DragAndDrop.css",
			});
			document.head.appendChild(css);

            let dropArea = Paw.nuevoElemento("div", "Arrastra acá las imagenes", {class:"drop-area"});

            contenedor.appendChild(dropArea);

			//Se desactivan los comportamientos por defecto
			['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
				dropArea.addEventListener(eventName, preventDefaults, false);
			  });
			  
			  function preventDefaults (e) {
				e.preventDefault();
				e.stopPropagation();
			  }

			//barra de progeso de carga de las imagenes
			let barraProgreso = Paw.nuevoElemento("progress", "", {id:"progress-bar",max:100, value:0}) 
			contenedor.appendChild(barraProgreso);
			let filesDone = 0
			let filesToDo = 0
			let progressBar = document.getElementById('progress-bar')

			function initializeProgress(numfiles) {
				progressBar.value = 0
				filesDone = 0
				filesToDo = numfiles
			}
			
			function progressDone() {
				filesDone++
				progressBar.value = filesDone / filesToDo * 100
			}
			
			  //Se agrega un feedback al usaurio de que mete o saca elementos en el drop area

			  ['dragenter', 'dragover'].forEach(eventName => {
				dropArea.addEventListener(eventName, highlight, false);
			  });
			  
			  ['dragleave', 'drop'].forEach(eventName => {
				dropArea.addEventListener(eventName, unhighlight, false);
			  })
			  
			  function highlight(e) {
				dropArea.classList.add('highlight');
			  }
			  
			  function unhighlight(e) {
				dropArea.classList.remove('highlight');
			  }
			  
			  dropArea.addEventListener('drop', handleDrop, false)

			//Cuando los archivos son dropeados dentro del drop area
			function handleDrop(e) {
			let dt = e.dataTransfer
			let files = dt.files

			handleFiles(files)

			function handleFiles(files) {
				//Se convierte la file list en un array para iterar mas facilmente
				files = [...files];
				initializeProgress(files.length); // <- Add this line
				files.forEach(uploadFile);
				files.forEach(previewFile);
			  }

			  function uploadFile(file) {
				
				let url = '/nuevo-turno/registrar'
				let formData = new FormData()
			  
				formData.append('file', file)
			  
				fetch(url, {
				  method: 'POST',
				  body: formData
				})
				.then(() => { console.info("Imagenes subida con exito") })
				.catch(() => { alert("No se pudo subir la imagen, error 500 de servidor") })
			  }

			  //Previsualizacion de las iamgenes

			  let galeria = Paw.nuevoElemento("div", "", {id:"galeria"});
			  contenedor.appendChild(galeria);

			  function previewFile(file) {
				let reader = new FileReader();
				reader.readAsDataURL(file);
				reader.onloadend = function() {
				  let img = document.createElement('img');
				  img.src = reader.result;
				  galeria.appendChild(img);
				}
			  }
			  




			  
			  
			}

			  


		} else {
				console.error("Elemento HTML para generar el drop area no encontrado");
		}
	}
		
}