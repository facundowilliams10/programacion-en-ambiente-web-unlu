class Paw {
    //es una funcion estatica para no tener que instanciar algun objeto para poder utilizarla

  //nuevoElemento("script", "", {scr: URL, name: "nombreDelScript"})  
 static nuevoElemento(tag, contenido, atributos = {} ) {
     let elemento = document.createElement(tag);
    
     for (const atributo in atributos) {
         elemento.setAttribute(atributo, atributos[atributo])
     }
    if (contenido.tagName)
        elemento.appendChild(contenido); 
    else
        elemento.appendChild(document.createTextNode(contenido));

    return elemento;
 }   

//Para cargar un script de forma vainilla,  sin AJAX o cosas por el estilo. 
//Cuando se necesite otro script sin tener que modificar el HTML
 static cargarScript (nombre, url, fnCallback = null) {
     let elemento = document.querySelector("script#" + nombre);
     if (!elemento) {
        //se crea el tag de script
        elemento = this.nuevoElemento("script","",{src: url, id: nombre});
        
        //lo siguiente debe hacerse siempre ANTES de insertar el elemento, 
        //ejecuta la funcion cuando termina de cargarse
        if (fnCallback)// el valor NULL se interpreta como FALSO
            elemento.addEventListener("load", fnCallback);
            
        //El broswer hace la peticion del recurso, y cuando termina de "loadearse" (o sea cargarse) se dispara el evento "load"

        document.head.appendChild(elemento);
     }

    return elemento;
 }
 static insertarLi(txt, lista = ".PawMenu ul", atributos = {}) {
    let ul = document.querySelector(lista);
    let li = document.createElement('li');
    let a = document.createElement('a');

    //Se setean los atributos
    for (const atributo in atributos) {
        a.setAttribute(atributo, atributos[atributo])
    }

    ul.appendChild(li);
    li.appendChild(a);
    a.appendChild(document.createTextNode(txt));
    return li;
}
static insertarUl(nodoPadre, atributos = {}) {
    let padre = document.querySelector(nodoPadre);
    let ul = padre.appendChild(document.createElement('ul'));
    for (const atributo in atributos) {
        ul.setAttribute(atributo, atributos[atributo])
    }
    return ul;
}
}