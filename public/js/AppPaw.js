// en caso que no tengamos la posibilidad de generar nuevos scrips
// cargariamos a las clases dentro del script principal


class AppPAW {
  constructor() {
    //se Inicializa la funcionalidad Menu

    window.addEventListener("resize", ()=>{
      //si se agranda la ventana se oculta el menu
      if (!window.matchMedia("(max-width: 768px)").matches ) {
        if (document.querySelector(".PawMenu"))
          document.querySelector(".PawMenu").classList.remove("PawMenu");
      }
      else
        document.querySelector("header nav").classList.add("PawMenu");
    }
    );

    //Se inicializa el menu SIEMPRE (tip de feedback)

    document.addEventListener("DOMContentLoaded", () => {
      Paw.cargarScript("PawMenu", "js/components/PawMenu.js", () => {
        let menu = new PawMenu("nav");
      });
      Paw.cargarScript("PawCarousel", "js/components/PawCarousel.js", () =>{
        let carousel = new PawCarousel();
      });
     
    Paw.cargarScript("PawAgenda", "js/components/PawAgenda.js", () =>{
      let agenda = new PawAgenda("form");
    });
    Paw.cargarScript("DragAndDrop", "js/components/DragAndDrop.js", () => {
      let dragAndDrop = new DragAndDrop(".Formulario_Nuevo_Turno");
    });

    //Carga del MVC Turnero
    Paw.cargarScript("TurnosModel", "js/components/Turnero/TurnosModel.js", ()=>{
      let turneroModel = new TurnosModel();
      Paw.cargarScript("PacienteView", "js/components/Turnero/PacienteView.js", ()=>{
        let turneroView = new PacienteView();
        Paw.cargarScript("TurneroController", "js/components/Turnero/TurneroController.js", ()=>{
          let turneroController = new TurneroController(turneroModel, turneroView, "#root");
        })
      })
    })

  });
}
}

// fecha = 2006-06-02
// NO ES PRECISO, FALLA POR UNOS AÑOS
function calculateAge(fecha){
  // today = new Date();
  // fecha = new Date(fecha);
  var _MS_PER_YEAR = 1000 * 60 * 60 * 24 * 31 * 12;
  a = new Date(fecha);
  b = new Date();
  var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
  var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
  var reply;
  reply = prompt("Hola tarola!");
  alert(reply);
  return Math.floor((utc2 - utc1) / _MS_PER_YEAR);
}

let App = new AppPAW();