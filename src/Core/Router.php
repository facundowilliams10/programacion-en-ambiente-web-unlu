<?php 

    namespace Paw\Core;
    
    use Exception;
    use Paw\Core\Exceptions\RouteNotFoundException;
    use Paw\Core\Request;
    use Paw\Core\Traits\TLogger;

    class Router{
        
        // Se incorpora el trait para que el roter pueda hacer logs.
        use TLogger;

        // Definiendo Array de Rutas.
        public array $routes =[
            'GET'  =>[],
            'POST' =>[],
        ];
        // Para las vistas especiales.
        public string $not_found = 'not_found';

        public string $internal_error = 'internal_error';

        
        /**
         * Constructor de Router--> cargo las rutas
         *
         * @return void
         */
        public function __construct(){

            // Rutas de Errores.
            $this->get($this->not_found,'ErrorController@notFound');
            $this->get($this->internal_error,'ErrorController@internalError');
        }


        /**
         * Carga todas las rutas del Sitio
         *
         * @return void
         */
        public function loadRoutes($path,$action, $http_method= 'GET'){
            $this->routes[$http_method][$path] = $action;
        }
        /**
         * Cargar Rutas de tipo GET
         *
         * @param  mixed $path
         * @param  mixed $action
         * @return void
         */
        public function get($path, $action){
            $this->loadRoutes($path, $action, 'GET');
        }
        /**
         * Cargar Rutas de tipo POST
         *
         * @param  mixed $path
         * @param  mixed $action
         * @return void
         */
        public function post($path, $action){
            $this->loadRoutes($path, $action, 'POST');
        } 
        /**
         * Funcion que retorna si existe o no la Ruta en el Array.
         *
         * @param  mixed $path
         * @param  mixed $action
         * @return boolean
         */
        public function exists($path, $http_method){
            return array_key_exists( $path, $this->routes[$http_method]);
        }        
        /**
         * Obtengo el controlador de la Ruta.
         *
         * @return array($controller, $method)
         */
        public function getController( $path, $http_method){
            if (! $this->exists($path, $http_method)) {
                // Si ruta no existe, lanzamos nuestra exception.
                throw new RouteNotFoundException("NO existe ruta para este Path.");
            }
            return  explode('@', $this->routes[$http_method][$path]);// separo el controlador del metodo y lo guardo
        }
        
        /**
         * Se encarga de llamar al metodo del Controlador  que corresponda.
         *
         * @return void
         */
        public function call($controller, $method){
            
            $controller_name = "Paw\\App\\Controllers\\{$controller}";
            $objController = new $controller_name;
            $objController->$method();
        }
        /**
         * Direcciona al path especifico
         *
         * @param  mixed $path
         * @return void
         */
        public function direct( Request $request ){
            try{
                list( $path,$http_method)    = $request->route();
                list( $controller, $method ) = $this->getController($path, $http_method); 
                $this->logger->info("Respuesta Exitosa: 200", ['Path' => $path, 'Method'=> $http_method, ]);
            }catch( RouteNotFoundException $e){

                list ($controller, $method ) = $this->getController( $this->not_found, "GET"); 
                $this->logger->debug("Status Code: 404 - Route Not Found", ['Error' => $e]);
            }catch ( Exception $e){

                list ($controller, $method ) = $this->getController( $this->internal_error, "GET"); 
                $this->logger->error("Status Code: 500 - Internal Server Error", ['Error' => $e]);
            }finally{
                $this->call($controller, $method);
            }
        }
    }