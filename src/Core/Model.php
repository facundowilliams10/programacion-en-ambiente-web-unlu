<?php

namespace Paw\Core;

use Paw\Core\Database\QueryBuilder;
use Paw\Core\Traits\TLogger;

class Model{
    use TLogger;

    // Seteamos el manejador de consultas.
    public function setQueryBuilder( QueryBuilder $qb ){
        $this->queryBuilder = $qb;
    }
}