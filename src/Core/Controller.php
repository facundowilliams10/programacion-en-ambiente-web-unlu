<?php 

namespace Paw\Core;

use Paw\Core\Model;
use Paw\Core\Database\QueryBuilder;

class Controller{
    public string $viewsDir; // Se crea porque en cada path se repiten las vistas.

    public ?string $modelName = null; 

    public $twig;

    public function __construct(){
        global $connection, $log, $twig;

        $this->twig = $twig;
        
        $this->viewsDir = __DIR__ . "/../App/views/";
         
         //Agregamos el menu Dinámico.
         $this->menu = [
            [
                'href' => '/listado-turnos',
                'name' => 'Listado de Turnos',
            ],
            [
                'href' => '/obras-sociales',
                'name' => 'Obras Sociales',
            ],
            [
                'href' => '/especialidades-profesionales',
                'name' => 'Profesionales y Especialidades',
            ],
            [
                'href' => '/noticias',
                'name' => 'Noticias',
            ],
            [
                'href' => '/institucional',
                'name' => 'Institucional',
            ],
            [
                'href' => '/turnero2',
                'name' => 'turnero2',
            ],
        ];

        if (! is_null($this->modelName)) {
            $qb = new QueryBuilder($connection);
            $qb->setLogger($log);
            $model = new $this->modelName;
            $model->setQueryBuilder($qb);
            $this->setModel( $model);

        }
    }

    public function setModel( $model ){
        $this->model = $model;
    }

    public function render($template, $params = []){
        $array = [
            'menu' => $this->menu
        ];
        foreach($params as $clave => $valor){
            $array[$clave] = $valor;
        }
        echo $this->twig->render($template, $array);
    }

}