<?php

namespace Paw\Core\Database;

use PDO;
use Paw\Core\Traits\TLogger;

class QueryBuilder{
    use TLogger;
    public function __construct( PDO $pdo ){
        $this->pdo=$pdo;
    }

    public function select($table , $params = [] ){
        $sentencia = $this->builWhere( $table, $params);
        $sentencia->setFetchMode(PDO::FETCH_ASSOC); //seteo modo de recuperacion
        $sentencia->execute();
        return $sentencia->fetchAll();
    }

    public function insert( $table , $fields_insert){
        if ( 0 < count($fields_insert) ){
            $fields="";
            $values="";
            $values_bind = array();
            $id=0;
            foreach( array_keys($fields_insert) as $key ){
                if( isset($fields_insert[$key]) ){
                    $fields .= $key;
                    $fields .= ( $fields_insert[$key] !== end($fields_insert) ) ? ', ' : '';

                    $values .= ':param' . $id;
                    $values .= ($fields_insert[$key] !== end($fields_insert) )? ', ' : '';


                    $values_bind = array_merge( $values_bind, array('param' . $id => $fields_insert[$key])) ;
                    $id++;
                }
            }

            $sql = "INSERT INTO {$table} ({$fields}) VALUES({$values})";
            $stmt= $this->pdo->prepare($sql);
            $stmt->execute($values_bind);
        }
    }
    public function update(){
        
    }
    public function delete(){
        
    }

    public function builWhere($table, $params ){
        $where = ""; 
        if( 0 < count($params) ){
            $id = 0;
            foreach( $params as $operador ){
                foreach( $operador as $field ){
                    foreach( $field as $f ){
                        if ( current(current($params[0])) !== $f){
                            $where .=' ' . key($operador) . ' ';
                        }

                        if ( isset($f[0]) && isset($f[1]) && isset($f[2])) {
                            $where       .= '( ' . $f[0] . ' ' . $f[1] . ' :param' . $id . ')';
                            $fields_bind[] = [':param' . $id => $f[2] ]; 
                            $id++;
                        }
                    }
                }
            }
        }else{
            $where = '1 = 1';
        }
        $query = "select * from {$table} where {$where}";
        $sentencia = $this->pdo->prepare($query);//preparar la sentencia
        
        if( 0 < count($params) ){
            foreach ( $fields_bind as $fb) {
                $sentencia->bindValue(strval(key($fb)),$fb[key($fb)]);
            }
        }
        return $sentencia;
    }

    // desde un id de especialidad, buscar los profesionales (nombre) asociados
    public function joinProf_EspProf($espID){
        $query = "select * from Esp_Prof join Profesionales 
                    on Esp_Prof.ID_Prof = Profesionales.ID_Prof 
                    where Esp_Prof.ID_Esp = " . $espID;
        $sentencia = $this->pdo->prepare($query);
        $sentencia->setFetchMode(PDO::FETCH_ASSOC); //seteo modo de recuperacion
        $sentencia->execute();
        return $sentencia->fetchAll();
    }

    // desde un id de profesional, buscar las especialidades (nombre) asociadas
    public function joinEsp_EspProf($profID){
        $query = "select * from Esp_Prof join Especialidades 
                    on Esp_Prof.ID_Esp = Especialidades.ID_Esp
                    where Esp_Prof.ID_Prof = " . $profID;
        $sentencia = $this->pdo->prepare($query);
        $sentencia->setFetchMode(PDO::FETCH_ASSOC); //seteo modo de recuperacion
        $sentencia->execute();
        return $sentencia->fetchAll();
    }

    //Devuelve el Horario disponible de un profesional de una especialidad en un dia y hora determinados
    public function getHorarioProf_Esp($id_esp,$id_prof,$h_ini,$dia){
        $query = 'SELECT *
        FROM Horario_Prof
        WHERE ID_Prof = :ID_Prof
        and ID_Esp = :ID_Esp
        and NameDia = :dia
        and :hora_ini between Hora_Inicio and Hora_Fin';
        $sentencia = $this->pdo->prepare($query);
        $sentencia->bindValue( ":ID_Prof", $id_prof );
        $sentencia->bindValue( ":ID_Esp", $id_esp );
        $sentencia->bindValue( ":dia", $dia );
        $sentencia->bindValue( ":hora_ini", $h_ini );
        $sentencia->setFetchMode(PDO::FETCH_ASSOC); //seteo modo de recuperacion
        $sentencia->execute();
        return $sentencia->fetchAll();
    }
}