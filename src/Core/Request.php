<?php 

namespace Paw\Core;

class Request{
    
    /**
     * Retorna el path de la peticion del usuario.
     *
     * @return void
     */
    public function uri(){
        return  parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    }    
    /**
     * Retorna el metodo http de la peticion del usuario.
     *
     * @return void
     */
    public function method(){
        return  $_SERVER['REQUEST_METHOD'];
    }
    /**
     * Retorna el path y el metdodo de la peticion del usuario.
     *
     * @return void
     */
    public function route(){
        return [
            $this->uri(),
            $this->method(),
        ];
    }

    public function get($key){
        return $_POST[$key] ?? $_GET[$key] ?? null;
    }
}