<?php
namespace Paw\Core\Traits;

use Monolog\logger;

trait TLogger{
    
    public $logger;

    public function setLogger( Logger $logger){
        $this->logger = $logger;
    }
}