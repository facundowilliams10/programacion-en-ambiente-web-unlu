<?php 

    namespace Paw\Core;

class Config{

    private array $configs;
    
    /**
     * El constructor seteará las configuraciones que se vayan incorporando.
     *
     * @return void
     */
    public function __construct(){
        $this->configs["LOG_LEVEL"] = getenv("LOG_LEVEL", "INFO" ); //si no esta seteada la variable por defecto sera INFO
        $path = getenv("LOG_PATH", "/logs/app.log"); //si no esta seteada la var por defecto /logs/app.log
        $this->configs["LOG_PATH"]  = $this->joinPaths('..', $path); 
        $this->configs["DB_ADAPTER"] = getenv("DB_ADAPTER");
        $this->configs["DB_HOSTNAME"] = getenv("DB_HOSTNAME");
        $this->configs["DB_DBNAME"] = getenv("DB_DBNAME");
        $this->configs["DB_PORT"] = getenv("DB_PORT");
        $this->configs["DB_CHARSET"] = getenv("DB_CHARSET");
        $this->configs["DB_USERNAME"] = getenv("DB_USERNAME");
        $this->configs["DB_PASSWORD"] = getenv("DB_PASSWORD");

        $this->configs["TWIG_TEMPLATES"] = getenv("TWIG_TEMPLATES");
        $this->configs["TWIG_CACHE"] = getenv("TWIG_CACHE");
    }
    
    /**
     * Obtener el valor de la variable de entorno
     *
     * @param  mixed $name
     * @return valor variable de entorno o nulo.
     */
    public function get( $name ){
        return $this->configs[ $name ] ?? null; //Si la variable no seteada, devuelve nulo.
    }
    
    /**
     * Se encarga en concatenar de manera correcta cualquier path que el usuario ingrese con la raiz del proyecto.
     *
     * @return void
     */
    public function joinPaths(){
        $paths = array();
        foreach ( func_get_args() as $arg ) {
            if ( '' !== $arg ) {
                $paths[] = $arg;
            }
        }
        return preg_replace('#/+#', '/', join( '/', $paths) );
    }
}