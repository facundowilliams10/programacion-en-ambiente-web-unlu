<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/profes-esp.css">
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php'; ?>
     <!-- El sigueinte nav es de referencia a las pag anteriores-->
     
     
     <!-- Profesionales y especialidades -->
    <?php if ( isset($tipo) ) {
            require 'parts/mensaje.php';
        }
    ?> 
     <main class="container-esp_pro">
        <nav>
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/profesionales-especialidades">Especialidades/Profesionales</a></li>
            </ul>
         </nav> 
       <h2>Especialidades y Profesionales</h2>
       <section class="container-esp">
           <h3>Lista de Especialidades</h3>
           <form action="/especialidades/buscar" method="POST" >
               <label for="especialidad-search">Ingrese Especialidad:</label>
               <fieldset>
               <input type="text" name="input-esp-search" id="especialidad-search" placeholder="Alergia e Inmunología" pattern="[a-zA-Z ]+">
               <input type="submit" id="btn-esp-search" value="Buscar">
               </fieldset>
           </form>

           <ul class="container-esp">
                <?php foreach($especialidades as $esp): ?>
                    <li> 
                        <a class="arrow-right" href="/especialidad?id=<?= $esp['ID_Esp'] ?>"> 
                            <?= $esp['Name'] ?> 
                        </a> 
                    </li>
                <?php endforeach; ?>
           </ul>
       </section>
       <section class="container-prof">
            <h3>Lista de Profesionales</h3>
            <form action="/profesionales/buscar" method="POST">
                <label for="profesional-search">Ingrese Nombre o Apellido del Profesional:</label>
                <fieldset>
                    <input type="text" name="input-prof-search" id="profesional-search" placeholder="Juan Pérez" pattern="[a-zA-Z ]+">
                    <input type="submit" id="btn-prof-search" value="Buscar">
                </fieldset>
            </form>
            <?php foreach($profesionales as $prof): ?>
                <article>
                    <img src="/imagenes/doctor.svg" alt="foto-del-profesional">
                    <h4> 
                        <a href="/profesional?id=<?= $prof['ID_Prof'] ?> ">
                            <?= $prof['Name'] ?>
                        </a> 
                    </h4>
                    <p> <?= $prof['DescripcionCargo'] ?> </p>
                </article>
            <?php endforeach; ?>
       </section>
       <section class="container-tel"> <!--seccion de numeros de contacto -->
           <span class="telefonoVerde"></span>
           <h2>Teléfonos</h2>
           <ul>
               <li>Tel Urgencias: 0800-XXX-XXXX</li>
               <li>Tel Urgencias: 0800-XXX-XXXX</li>
           </ul>
       </section>
   </main>
    

    <!-- Footer -->
    <?php require 'parts/footer.php'; ?>
</body>
</html>