<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/register.css">
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>
    <main>
    <?php if ( isset($tipo) ) {
                    require 'parts/mensaje.php';
            }
    ?> 
    <form action="/registrarse" method="POST">
        <label for="email">Ingrese Correo Electrónico:</label>
        <input type="text" id="email" name="Email" required>
        <label for="password">Ingrese Contraseña:</label>
        <input type="password" id="password" name="Password" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required>
        <input type="submit" name="register" value="Registrar">
    </form>
    </main>
    <!-- Footer -->
    <?php require 'parts/footer.php'; ?>
</body>
</html>