<header>  <!--los elementos agrupados tienen una semantica-->
        <h1 class="logo-link">  
            <a href="/">                            
                <span class="logo"></span>
                UNLuPAW Medical Group
            </a>
        </h1>
        <ul>
            <li><span>Orientacion: 0810-444-7700</span></li>
            <li><span>Urgencias: 0800-777-7800</span></li>
            <li><span>Informacion al socio: 0810-444-7700</span></li>
        </ul> 

        <nav>
            <ul>
                <?php foreach ( $this->menu as $item) :?>
                    <li><a href="<?=$item['href'] ?>"><?=$item['name'] ?></a></li>
                <?php endforeach;?>
            </ul>
        </nav>
       <!-- Comento este elemento porque rompe la pagina. Atte. Facundo
       TODO: Definir si lo sacamos, total la opcion puede estar dentor del menu hamburguesa mmmmmm
        <a href="/ingresar"> <span class="login-menu"></span></a>     
        <h2>Login Menú</h2>
       -->

    </header> 