<link rel="stylesheet" href="/assets/css/reset.css">
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Angel Gallozo, Facundo Williams y Sebstián Marchetti">
<meta name="description" content="This web page is for the course Programacion en ambiente web, Universidad Nacional de Lujan">
<meta name="keywords" content="HTML, CSS, paw, unlu, html5">
<link rel="icon" href="/imagenes/logoPixelado.ico"> 
<link rel="stylesheet" href="/assets/css/styles.css">
<link rel="stylesheet" href="/assets/css/profes-esp.css">
<title><?php echo $title; ?></title>
<script src="/js/AppPaw.js"> </script>
<script src="/js/components/Paw.js"> </script>