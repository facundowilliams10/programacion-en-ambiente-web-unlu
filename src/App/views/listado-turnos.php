<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/listado-turnos.css">
    <script src="/js/Agenda.js"> </script>
</head>

<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>

    <!-- Listado de turnos-->
    <main>
        <nav>
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/listado-turnos">Listado de Turnos</a></li>
            </ul>
         </nav> 

        <a href="/nuevo-turno" class="button">Solicitar Nuevo Turno </a>

        <section class="filtro-section">
            <h2>Listado de Turnos:</h2>
            <h3>Filtrar: </h3>
            <label for="filtro">Filtrar por algún campo</label>
            <input type="text" id="filtro" placeholder="15/05, revisión, oftalmología, Fernandez, Pilar.." title="Type in a name">
        </section>

        <section class="turnos-section">
            <h4>Turnos</h4>
            <article>
                <h4>Turno:</h4>
                <ul>
                    <li>Fecha y Hora</li>
                    <li>Práctica</li>
                    <li>Especialidad</li>
                    <li>Profesional</li>
                    <li>Ubicación</li>
                </ul>
            </article>
            <article>
                <h4>Turno:</h4>
                <ul>
                    <li>Fecha y Hora</li>
                    <li>Práctica</li>
                    <li>Especialidad</li>
                    <li>Profesional</li>
                    <li>Ubicación</li>
                </ul>
            </article>
            <article>
                <h4>Turno:</h4>
                <ul>
                    <li>Fecha y Hora</li>
                    <li>Práctica</li>
                    <li>Especialidad</li>
                    <li>Profesional</li>
                    <li>Ubicación</li>
                </ul>
            </article>
            <article>
                <h4>Turno:</h4>
                <ul>
                    <li>Fecha y Hora</li>
                    <li>Práctica</li>
                    <li>Especialidad</li>
                    <li>Profesional</li>
                    <li>Ubicación</li>
                </ul>
            </article>
            <article>
                <h4>Turno:</h4>
                <ul>
                    <li>Fecha y Hora</li>
                    <li>Práctica</li>
                    <li>Especialidad</li>
                    <li>Profesional</li>
                    <li>Ubicación</li>
                </ul>
            </article>
            <article>
                <h4>Turno:</h4>
                <ul>
                    <li>Fecha y Hora</li>
                    <li>Práctica</li>
                    <li>Especialidad</li>
                    <li>Profesional</li>
                    <li>Ubicación</li>
                </ul>
            </article>
        </section>


        <section class="container-tel"> <!--seccion de numeros de contacto -->
            <span class="telefonoVerde"></span>
            <h2>Teléfonos</h2>
            <ul>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
            </ul>
        </section>
    </main>
    
   

     <!-- Footer -->
     <?php require 'parts/footer.php'; ?>

</body>
</html>
