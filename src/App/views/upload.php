<?php

/* VISTA DE EJEMLPO PARA LLAMAR AL SCRIPT

<!DOCTYPE html>
<html>
<body>

<form action="upload.php" method="post" enctype="multipart/form-data">
  Select image to upload:
  <input type="file" name="fileToUpload" id="fileToUpload">
  <input type="submit" value="Upload Image" name="submit">
</form>

</body>
</html>

*/

// $fileInfo = pathinfo($inp_path["name"]);

// move_uploaded_file($inp_path["tmp_name"],
//     "private/" . uniqid() . '.' . $fileInfo['extension']);



$target_dir = "../private/";
//Se le da un ID unico como nombre al archivo, basado en el tiemp actual
//Esto es para que se puedan subir archivos originalmente con el mismo nombre
// $time = time();
// ../../private/ejemplo.png

$path = pathinfo($inp_path["name"]);
$filename = time();
// $imageFileType = strtolower(pathinfo(basename($inp_path["name"]),PATHINFO_EXTENSION));
$imageFileType = $path['extension'];
$target_file = $target_dir . $filename . '.' . $imageFileType;
$uploadOk = 1;

// Validamos que el archivo sea una imagen
if(isset($_POST["Enviar"])) {
    $check = getimagesize($inp_path["tmp_name"]);
    if($check !== false) {
        // Se valida que el archivo no exista
        if (!file_exists($target_file)) {
            // Se valida el tamaño del archivo
            if (!($inp_path["size"] > 500000)) {
                // Se valida el formato del archivo sea de tipo imagen
                if(($imageFileType == "jpg") || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "gif" ) {
                    $flag = move_uploaded_file($inp_path["tmp_name"], $target_file);
                    if(!$flag) {
                        $descripcion = "Error, ha ocurrido un problema al tratar de subir el archivo";
                        $uploadOk = 0;
                    }
                }else{
                    $descripcion = "Error, solamente se admiten archivos del tipo JPG, JPEG, PNG y GIF";
                    $uploadOk = 0;
                }
            }else{
                $descripcion = "Error, archivo demasiado grande";
                $uploadOk = 0;
            }
        }else{
            $descripcion = "Error, el archivo ya existe";
            $uploadOk = 0;
        }
    } else {
        $descripcion = "El archivo no es una imagen.";
        $uploadOk = 0;
    }
}