<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/obrasSociales.css">
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>


      <!-- El sigueinte nav es de referencia a las pag anteriores-->
      
      
      <!-- Obras sociales -->
    <main class="main-OS">
        <?php 
            if ( isset($tipo) ) {
                require 'parts/mensaje.php';
            }
        ?> 
        <nav>
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/obras-sociales">Obras Sociales</a></li>
            </ul>
        </nav>
        <section class="container-main-os">
            <h2>Obras Sociales</h2>
            <form action="/obras-sociales/buscar" method="POST" class="os-search-form">
                <label for="input-OS-search">Ingrese la obra social a buscar</label>
                <fieldset class="os-search-set">
                    <input type="text" name="input-OS-search" id="input-OS-search" placeholder="OSDE, OSPIN, etc" pattern="[a-zA-Z ]+">
                    <button type="submit" class="botonBusqueda"></button>
                </fieldset>
            </form>  
            
            <ul class="list-result-os">
                <?php foreach($obrasSociales as $obraS): ?>
                    <li> <?= $obraS->fields['NameOS'] ?> </li>
                <?php endforeach ?>
            </ul>
        </section>
        <section class="container-tel"> <!--seccion de numeros de contacto -->
            <span class="telefonoVerde"></span>
            <h2>Teléfonos</h2>
            <ul>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
            </ul>
        </section>
    </main>

    

    <!-- Footer -->
    <?php require 'parts/footer.php'; ?>
</body>
</html>