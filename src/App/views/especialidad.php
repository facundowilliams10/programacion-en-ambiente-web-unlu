<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/profes-esp.css">
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>
     <!-- El sigueinte nav es de referencia a las pag anteriores-->
     
     
     <!-- Profesionales y especialidades -->
     <main class="container-especialidad">
        <nav>
            <ul>
                <li><a href="/index.html">Home</a></li>
                <li><a href="/html/profesionales-especialidades.html">Especialidades/Profesionales</a></li>
                <li><a href="/html/especialidad.html">Especialidad</a></li>
            </ul>
         </nav> 
         <h2>Especialidades</h2>
         <section>
            <h3> <?php echo $especialidad['Name'];?> </h3>
            <section class="info-especialidad descripcion">
                <h4>Descripcion</h4>
                <p><?php echo $especialidad['Descripcion']; ?></p>
            </section>
            <a class="button btn-especialidad" href="/nuevo-turno?id_esp=<?php echo $especialidad['ID_Esp'];?>">Sacar Turno</a>
            <section class="info-especialidad profesionales">
                <h4>Profesionales Asociados</h4>
                <ul>
                    <?php foreach($profAsociados as $prof): ?>
                        <li> <a class="arrow-right" href="/profesional?id=<?= $prof['ID_Prof'] ?> "> <?= $prof["Name"] ?> </a> </li>
                    <?php endforeach?>
                </ul>
            </section>
         </section>
   </main>
    

    <!-- Footer -->
    <?php require 'parts/footer.php';?>
</body>
</html>