<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>
    <link rel="stylesheet" href="/assets/css/institucional.css">
     <!-- El sigueinte nav es de referencia a las pag anteriores-->
    <main>
        <?php
        /*Probando los mensajes de aviso al usaurio :D
            $titulo="Probando";
            $descripcion="Estamos probando una alerta :D";
            $tipo=2;
            require 'parts/mensaje.php';
            */
            ?>
        <nav>
           <ul>
               <li><a href="/">Home</a></li>
               <li><a href="/institucional">Institucional</a></li>
           </ul>
        </nav> 
        <section>
            <h2>Tenemos...</h2>
            <section>
                <h3>Una misión</h3>
                <p>
                    Brindarte la mejor atención.
                </p>
            </section>
            <section>
                <h3>Valores</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                <ul>
                    <li>Calidad de atención</li>
                    <li>Compromiso</li>
                    <li>Eficiencia</li>
                </ul>      
            </section>
            <section class="directorio">
                <h3>Directorio</h3>
                <!-- Card de directivos -->
                <section class="Directores">
                    <h4>Directores</h4>
                    <figure class="Director">
                        <img src="/imagenes/img_avatar_hombre.png">
                        <figcaption>Javier - Director</figcaption>
                      </figure> 
                </section>
                <section class="Gerente">
                    <h4>Gerentes</h4>
                    <figure>
                        <img src="/imagenes/img_avatar_hombre.png">
                        <figcaption>Pablo - Gerente</figcaption>
                    </figure> 
                    <figure>
                        <img src="/imagenes/img_avatar_mujer.png">
                        <figcaption>Micaela - Gerente</figcaption>
                    </figure> 
                </section>
                <section class="Lider">
                    <h4>Lideres</h4>
                    <figure>
                        <img src="/imagenes/img_avatar_hombre.png">
                        <figcaption>Santiago - Líder</figcaption>
                    </figure> 
                    <figure>
                        <img src="/imagenes/img_avatar_hombre.png">
                        <figcaption>Tomás - Líder</figcaption>
                    </figure> 
                </section>
            </section>
        </section>
        <section class="container-tel"> <!--seccion de numeros de contacto -->
            <span class="telefonoVerde"></span>
            <h2>Teléfonos</h2>
            <ul>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
            </ul>
        </section>
    </main> 
  <!-- Footer -->
  <?php require 'parts/footer.php'; ?></footer>
</body>
</html>
