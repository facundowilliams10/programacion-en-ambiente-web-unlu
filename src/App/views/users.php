<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>
    <main>
        <table>
            <thead>
                <tr>
                    <th>email</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user) : ?>
                <tr>
                    <td><?php echo $user->fields["Email"]; ?></td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </main>
    <!-- Footer -->
    <?php require 'parts/footer.php'; ?>
</body>
</html>