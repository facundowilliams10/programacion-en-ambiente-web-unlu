<!DOCTYPE html>
<html lang="es">
<head>
    <title>500 Error en el servidor</title>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/errores.css">
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php'?>
    <main>
    <section>
            <h2>¡Error en el servidor!</h2>
            <p>Se ha producido un error en nuestros servidores</p>
            <p>Estamos trabajando en poder solucionarlo</p>
            <p>Código de error: 500</p>
            <a href="/" class="button">Ir al Inicio</a>
        </section>    </main>
    <!-- Footer -->
    <?php require 'parts/footer.php'; ?>
</body>
</html>