<!DOCTYPE html>
<html lang="es">
<head>
    <?php require 'parts/head.php';?>
    <link rel="stylesheet" href="/assets/css/nuevoTurno.css">
</head>
<body>
    <!--Cabecera-->
    <?php require 'parts/header.php';?>
     <!-- El sigueinte nav es de referencia a las pag anteriores-->
     
     
     <!-- Nuevo Turno -->
    <main >
        <nav>
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="/nuevo-turno">Solicitud Nuevo Turno</a></li>
            </ul>
        </nav> 
        <?php if ( isset($tipo) ) {
                    require 'parts/mensaje.php';
            }
        ?>
        <h2>Solicitud de Turno Nuevo</h2>
        <p>Utilice los iconos de calendario para ingresar fechas</p>
        <form action="/nuevo-turno/registrar" method="POST" name="Formulario_Nuevo_Turno" class="Formulario_Nuevo_Turno" enctype="multipart/form-data">
            <label>Nombre y Apellido:
                <input type="text" id="input_NomApe" name="input_NomApe" required placeholder="Miguel Torres">
            </label>

            <label for="input_Mail">Correo Electronico
                <input type="email" id="input_Mail" name="input_Mail" required placeholder="ejemplo@dominio.com">
            </label>

            <label for="input_Tel">Telefono:
                <input type="tel" id="input_Tel" name="input_Tel"  placeholder="2323 XXXXXX" patter= "/[0-9]+$/" required>
            </label>


            <fieldset class="NAC-AGE">
                <!-- inpu_Edad.value = funcion de php? -->
                <label for="input_Nacimiento" class="label-nac" 
                    oninput="input_Edad.value=1">Fecha de Nacimiento:
                    <input type="date" id="input_Nacimiento" name="input_Nacimiento" 
                    max="<?php $date = getdate()['year'] . '-' . str_pad(getdate()['mon'], 2, "0", STR_PAD_LEFT) . '-' . str_pad(getdate()['mday'], 2, "0", STR_PAD_LEFT);
                            echo date('Y-m-d', strtotime($date. ' - 15 year'));?>"
                    min="<?php
                            echo date('Y-m-d', strtotime($date. ' - 100 year'));
                            ?>"
                     required>
                </label>
                <label for="input_Edad" class="label-output">Edad:
                    <output id="input_Edad" name="input_Edad"></output>
                </label>
            </fieldset>

            <label for="especialidad">Ingresar nombre de la Especialidad
                <input list="especialidades" name="especialidad" id="especialidad"
                value="<?php echo ( (1 === count($especialidades)) ? $especialidades[0]['Name'] : '' );?>" <?php echo ( (1 === count($especialidades)) ? 'readonly' : '' );?> 
                data-id_esp="<?php echo ( (1 === count($especialidades)) ? $especialidades[0]['ID_Esp'] : '' );?>" required>
            </label>
            <datalist id="especialidades">
                <?php if ( 1 < count($especialidades) ) {
                    foreach ($especialidades as $esp) :?>
                        <option value="<?php echo $esp['Name']; ?>" data-id_esp="<?php echo $esp['ID_Esp']?>"> <?php echo $esp['Name']; ?> </option>
                    <?php endforeach;
                }?>       
            </datalist>
            
            <label for="profesional">Ingresar nombre del Profesional
                <input list="profesionales" name="profesional" id="profesional"
                value="<?php echo ( (1 === count($profesionales)) ? $profesionales[0]['Name'] : '' );?>" <?php echo ( (1 === count($profesionales)) ? 'readonly' : '' );?> 
                data-id_prof="<?php echo ( (1 === count($profesionales)) ? $profesionales[0]['ID_Prof'] : '' );?>" required>
            </label>
            <datalist id="profesionales">
                <?php if ( 1 < count($profesionales) ) {
                    foreach ($profesionales as $prof) :?>
                        <option value="<?php echo $prof['Name']; ?>" data-id_prof="<?php echo $prof['ID_Prof']?>" > <?php echo $prof['Name']; ?> </option>
                <?php endforeach;
                }?>
            </datalist>

            
            <label for="FechaTurno"> Seleccionar la Fecha del Turno
                <input type="date" id="FechaTurno" name="FechaTurno" min="<?php echo ( getdate()['year'] . '-' . str_pad(getdate()['mon'], 2, "0", STR_PAD_LEFT) . '-' . str_pad(getdate()['mday'], 2, "0", STR_PAD_LEFT)); ?>" required>
            </label>

            <label for="HoraTurno"> Seleccionar la Hora del Turno
                <input type="time" id="HoraTurno" name="HoraTurno" required>
            </label>
            
            <label for="fileToUpload"> Aqui para subir una imagen
                <input type="file" name="fileToUpload" id="fileToUpload" required>
            </label>
            

            <fieldset class="buttons-form">
                <input type="reset" id="inputReset" value="Limpiar">
                <input type="submit" id="inputSubmit" value="Enviar" name="Enviar">
            </fieldset>
            <p>(Todos los campos son obligatorios)</p>
        </form>



        <section class="container-tel"> <!--seccion de numeros de contacto -->
            <span class="telefonoVerde"></span>
            <h2>Teléfonos</h2>
            <ul>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
                <li>Tel Urgencias: 0800-XXX-XXXX</li>
            </ul>
        </section>
    </main>

    

   <!-- Footer -->
   <?php require 'parts/footer.php'; ?>
</body>
</html>