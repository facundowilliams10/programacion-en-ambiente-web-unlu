<?php
namespace Paw\App\Models;

use Paw\Core\Model;

class Noticia extends Model{

    public $table = 'Noticias';

    public $fields = [
        "ID_Noticia" => null,
        "Titulo" => null,
        "Fecha" => null,
        "Imagen_Large" => null,
        "Imagen_Small" => null,
        "Texto_Completo" => null,
        "Descripcion" => null,
    ];

    
    public function setID_Noticia( string $id_noticia){
        $this->fields['ID_Noticia'] = $id_noticia;
    }

    public function setTitulo( string $titulo){
        $this->fields['Titulo'] = $titulo;
    }

    public function setFecha( string $fecha){
        $this->fields['Fecha'] = $fecha;
    }

    public function setImagen_Large( string $imagen_Large){
        $this->fields['Imagen_Large'] = $imagen_Large;
    }

    public function setImagen_Small( string $imagen_Small){
        $this->fields['Imagen_Small'] = $imagen_Small;
    }

    public function setTexto_Completo( string $texto_Completo){
        $this->fields['Texto_Completo'] = $texto_Completo;
    }

    public function setDescripcion( string $descripcion){
        $this->fields['Descripcion'] = $descripcion;
    }

    public function set( array $values){
        foreach ( array_keys($this->fields) as $field ){
            if (! isset( $values[$field]) ){
                continue;
            }

            $method = "set" . ucfirst($field);
            $this->$method( $values[$field] );
        }
    }

    public function getCard(){
        return array( 
                'ID_Noticia' => $this->fields['ID_Noticia'], 
                'Titulo' => $this->fields['Titulo'],
                'Imagen_Small' => $this->fields['Imagen_Small'],
                'Descripcion' => $this->fields['Descripcion']
            );
    }

    // recupera una noticia de la BD y carga sus datos en la instancia
    public function load($id){
        // Obtener el profesional
        $params = [ ['and' => [ ['ID_Noticia','=', $id ] ] ] ];
        $record = current($this->queryBuilder->select($this->table, $params));
        $this->set($record);
    }

}