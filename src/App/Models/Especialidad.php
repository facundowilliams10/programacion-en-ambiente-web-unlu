<?php
namespace Paw\App\Models;

use Paw\Core\Model;

class Especialidad extends Model{

    public $table = 'Especialidades';

    public $fields = [
        "ID_Esp"      => null,
        "Name"        => null,
        "Descripcion" => null,
    ];

    public function setId_esp( $esp_id){
        $this->fields['ID_Esp'] = $esp_id;
    }

    public function setName( string $esp_name){
        if ( strlen( $esp_name ) > 60 ) {
            throw new InvalidFormatValueException("El nombre del Autor no debe superar los 60 caracteres.");
        }
        $this->fields['Name'] = $esp_name;
    }
    
    public function setDescripcion( $esp_desc){
        $this->fields['Descripcion'] = $esp_desc;
    }

    public function set( array $values){
        foreach ( array_keys($this->fields) as $field ){
            if (! isset( $values[$field]) ){
                continue;
            }
            $method = "set" . ucfirst($field);
            if (method_exists($this,$method)){
                $this->$method( $values[$field] );
            }
        }
    }

    public function load($id){
        // Obtener el profesional
        $params = [ ['and' => [ ['ID_Esp','=', $id ] ] ] ];
        $record = current($this->queryBuilder->select($this->table, $params));
        $this->set($record);
    }

}