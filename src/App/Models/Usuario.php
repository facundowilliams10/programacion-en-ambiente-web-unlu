<?php 

namespace Paw\App\Models;

use Paw\Core\Model;
use Paw\Core\Exceptions\InvalidFormatValueException;
use Exception;

class Usuario extends Model{ 
    public $table = 'Usuarios';
    
    public $fields = [
        "Email"    => null,
        "Password" => null,
        "Created" => null,
    ];
    
    public function setCreated( string $created_timestamp){
        $this->fields['Created'] = $created_timestamp;
    }
    
    public function setEmail( string $usr_email){
        if ( ! filter_var( $usr_email, FILTER_VALIDATE_EMAIL ) ) {
            throw new InvalidFormatValueException("Formato del Email NO Valido.");
        }
        $this->fields['Email'] = $usr_email;
    }
    
    public function setPassword( string $usr_pass){
        $this->fields['Password'] = $usr_pass;
    }
  
    public function set( array $values){
        foreach ( array_keys($this->fields) as $field ){
            if (! isset( $values[$field]) ){
                continue;
            }

            $method = "set" . ucfirst($field);
            $this->$method( $values[$field] );
        }
    }

    public function existUser(){
        //Verificar si el email existe en la BD
        $user = $this->queryBuilder->select($this->table);
        $usersCollection = [];
        foreach ($users as $user){
            $newUser = new Usuario;
            $newUser->set($user);
            $usersCollection[] = $newUser;
        }
        return $usersCollection;
    }

}