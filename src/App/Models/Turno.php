<?php 

namespace Paw\App\Models;

use Paw\Core\Model;

class Turno extends Model{ 
    public $table = 'Turnos';
    
    public $fields = [
        "NameDia"       => null,
        "ID_Horario" => null,
        "ID_Prof"    => null,
        "ID_Esp" => null,
        "ID_User" => null,
        "Fecha" => null,
        "Path" => null,
    ];

    
    public function setNameDia( $name_dia){
        $this->fields['NameDia'] = $name_dia;
    }
    public function setHorario( $id_horario){
        $this->fields['ID_Horario'] = $id_horario;
    }
    public function setProfesional( $id_prof){
        $this->fields['ID_Prof'] = $id_prof;
    }
    public function setEspecialidad( $id_esp){
        $this->fields['ID_Esp'] = $id_esp;
    }
    public function setUsuario( $id_usr){
        $this->fields['ID_User'] = $id_usr;
    }
    public function setFecha( $id_fecha){
        $this->fields['Fecha'] = $id_fecha;
    }
    public function setPath($path){
        $this->fields['Path'] = $path;
    }
}