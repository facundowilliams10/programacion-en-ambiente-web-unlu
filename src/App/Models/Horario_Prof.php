<?php
namespace Paw\App\Models;

use Paw\Core\Model;

class Horario_Prof extends Model{

    public $table = 'Horario_Prof';

    public $fields = [
        "NameDia" => null,
        "Hora_Inicio" => null,
        "Hora_Fin" => null,
        "ID_Prof" => null,
    ];
    
    public function setNamedia( string $NameDia){
        $this->fields['NameDia'] = $NameDia;
    }

    public function setHora_Inicio( $Hora_Inicio){
        $this->fields['Hora_Inicio'] = substr($Hora_Inicio, 0, -3);
    }
    public function setHora_Fin( $Hora_Fin){
        $this->fields['Hora_Fin'] = substr($Hora_Fin, 0, -3);
    }

    public function setId_prof($ID_Prof){
        $this->fields['ID_Prof'] = $ID_Prof;
    }
  
    public function set( array $values){
        foreach ( array_keys($this->fields) as $field ){
            if (! isset( $values[$field]) ){
                continue;
            }

            $method = "set" . ucfirst($field);
            $this->$method( $values[$field] );
        }
    }


}