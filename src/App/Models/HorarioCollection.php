<?php

namespace Paw\App\Models;

use Paw\Core\Model;
use Paw\App\Models\Horario;

class HorarioCollection extends Model{

    public $table = 'Horarios';

    public function getAll(){
        $horarios = $this->queryBuilder->select($this->table);    
        $horarioCollection = [];
        foreach($horarios as $horario){
            $newHorario = new Horario;
            $newHorario->set($horario);
            $horarioCollection[] = $newHorario;
        }
        return $horarioCollection;
    }

    //Retorna los horarios por un ID_Horario
    public function getHorariosID($NameDia, $Hora_ini,$Hora_fin){
        $params = [ ['and' => [ ['NameDia','=',$NameDia ],['Hora_inicio','=',$Hora_ini ],['Hora_Fin','=',$Hora_fin ] ] ] ];
        $horarios = $this->queryBuilder->select($this->table, $params);  
        $horarioCollection = [];
        foreach($horarios as $horario){
            $newHorario = new Horario;
            $newHorario->set($horario);
            $horarioCollection[] = $newHorario;
        }
        return $horarioCollection;
    }

}