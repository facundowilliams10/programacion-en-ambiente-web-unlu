<?php
namespace Paw\App\Models;

use Paw\Core\Model;

class EspProf extends Model{

    public $table = 'Esp_Prof';

    public $fields = [
        "ID_Esp" => null,
        "ID_Prof" => null,
    ];

    public function setId_esp( string $ID_Esp){
        $this->fields['ID_Esp'] = $ID_Esp;
    }

    public function setId_prof( $ID_Prof){
        $this->fields['ID_Prof'] = $ID_Prof;
    }
  
    public function set( array $values){
        foreach ( array_keys($this->fields) as $field ){
            if (! isset( $values[$field]) ){
                continue;
            }

            $method = "set" . ucfirst($field);
            $this->$method( $values[$field] );
        }
    }


}