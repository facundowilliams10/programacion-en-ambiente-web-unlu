<?php

namespace Paw\App\Models;

use Paw\Core\Model;

use Paw\App\Models\Turno;
use Paw\App\Models\Especialidad;
use Paw\App\Models\Profesionales;

class TurnosCollection extends Model{ 

    public $tbl_turnos = 'Turnos';

    public function getAll(){
    }


    public function insert($fecha,$h_ini,$id_prof,$id_esp,$email,$path){
        $h_fin = date('H:i:s', strtotime('+1 hour',strtotime($h_ini)));
        $insert_fields = [ 
            'Fecha'    => $fecha,
            'Hora_Inicio' => $h_ini,
            'Hora_Fin' => date( 'H:i:s',strtotime($h_fin)),
            'ID_Prof' => $id_prof,
            'ID_Esp' => $id_esp,
            'Email' => $email,
            'Path' => $path,
        ];
        $this->queryBuilder->insert($this->tbl_turnos, $insert_fields);
    }

    // Devuelve true o false, si existe el horario de un profesional en un dia determinado
    public function existHorario($id_esp,$id_prof,$h_ini,$dia){
        $horario = $this->queryBuilder->getHorarioProf_Esp($id_esp, $id_prof, $h_ini, $dia);
        return ( 0 < count($horario) );
    }

    // Devuelve true o false, si existe turno de un profesional en un dia determinado
    public function existTurno( $fecha, $id_esp, $id_prof, $h_ini){
        $conditions = [ ['and' => [ ['Fecha','=',$fecha ], ['ID_Esp','=',$id_esp ], ['ID_Prof','=',$id_prof ], ['Hora_Inicio','=',$h_ini ] ] ] ];
        $turno = $this->queryBuilder->select($this->tbl_turnos,$conditions);
        return ( 0 < count($turno) );
    }
}