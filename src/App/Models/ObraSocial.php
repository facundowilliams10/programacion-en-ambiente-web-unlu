<?php
namespace Paw\App\Models;

use Paw\Core\Model;

class ObraSocial extends Model{

    public $table = 'ObrasSociales';

    public $fields = [
        "NameOS" => null,
    ];

    
    public function setNameos( string $prof_name){
        if ( strlen( $prof_name ) > 60 ) {
            throw new InvalidFormatValueException("El nombre del Autor no debe superar los 60 caracteres.");
        }
        $this->fields['NameOS'] = $prof_name;
    }

    public function set( array $values){
        foreach ( array_keys($this->fields) as $field ){
            if (! isset( $values[$field]) ){
                continue;
            }

            $method = "set" . ucfirst($field);
            $this->$method( $values[$field] );
        }
    }

}