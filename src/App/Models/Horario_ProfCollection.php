<?php

namespace Paw\App\Models;

use Paw\Core\Model;
use Paw\App\Models\Horario_Prof;

class Horario_ProfCollection extends Model{

    public $table = 'Horario_Prof';

    public function getAll(){
        $horarios_prof = $this->queryBuilder->select($this->table);    
        $horario_PCollection = [];
        foreach($horarios_prof as $horarioP){
            $newHorarioP = new Horario;
            $newHorarioP->set($horarioP);
            $horario_PCollection[] = $newHorarioP;
        }
        return $horario_PCollection;
    }

    //Retorna los horarios de un Profesional
    public function getHorario_ProfID($ID_Prof){
        $params = [ ['and' => [ ['ID_Prof','=',$ID_Prof ] ] ] ];
        $horarios_prof = $this->queryBuilder->select($this->table, $params);  
        $horario_PCollection = [];
        foreach($horarios_prof as $horarioP){
            $newHorarioP = new Horario_Prof;
            $newHorarioP->set($horarioP);
            $horario_PCollection[] = $newHorarioP;
        }
        return $horario_PCollection;
    }

}