<?php
namespace Paw\App\Models;

use Paw\Core\Model;

class Horario extends Model{

    public $table = 'Horarios';

    public $fields = [
        "NameDia" => null,
        "Hora_Inicio" => null,
        "Hora_Fin" => null,
    ];
    
    public function setNameDia( string $NameDia){
        $this->fields['NameDia'] = $NameDia;
    }

    public function setHora_inicio( $Hora_Inicio){
        $this->fields['Hora_Inicio'] = substr($Hora_Inicio, 0, -3);
    }

    public function setHora_Fin($Hora_Fin){
        $this->fields['Hora_Fin'] = substr($Hora_Fin, 0, -3);
    }
  
    public function set( array $values){
        foreach ( array_keys($this->fields) as $field ){
            if (! isset( $values[$field]) ){
                continue;
            }

            $method = "set" . ucfirst($field);
            $this->$method( $values[$field] );
        }
    }


}