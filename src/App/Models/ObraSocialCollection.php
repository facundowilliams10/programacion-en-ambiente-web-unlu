<?php

namespace Paw\App\Models;

use Paw\Core\Model;
use Paw\App\Models\ObraSocial;

class ObraSocialCollection extends Model{

    public $table = 'ObrasSociales';

    public function getAll(){
        $obrasSociales = $this->queryBuilder->select($this->table);    
        $OS_Collection = [];
        foreach($obrasSociales as $OS){
            $newOS = new ObraSocial;
            $newOS->set($OS);
            $OS_Collection[] = $newOS;
        }
        return $OS_Collection;
    }

    // Devuelve una lista con las OS que tengan el texto en el nmobre
    public function getBuscar(string $namePart){
        $conditions = [ ['and' => [ ['NameOS','like', '%' . $namePart . '%'] ] ] ];
        $obras = $this->queryBuilder->select($this->table, $conditions);
        $obrasCollection = [];
        foreach($obras as $obra){
            $newObra = new ObraSocial;
            $newObra->set($obra);
            $obrasCollection[] = $newObra;
        }
        return $obrasCollection;
    }

}