<?php

namespace Paw\App\Models;

use Paw\Core\Model;
use Paw\App\Models\Profesional;

class ProfesionalCollection extends Model{

    public $table = 'Profesionales';

    public function getAll(){
        // Crear tantos Autor como filas d ela tabla autores
        $profesionals = $this->queryBuilder->select($this->table);    
        $profesionalsCollection = [];
        foreach($profesionals as $prof){
            $newProf = new Profesional;
            $newProf->set($prof);
            $profesionalsCollection[] = $newProf->fields;
        }
        return $profesionalsCollection;
    }

    // Devuelve una lista cuyos nombres matcheen lo que recibe
    public function getBuscar($conditions){
        $profesionals = $this->queryBuilder->select($this->table, $conditions);
        $profesionalsCollection = [];
        foreach($profesionals as $prof){
            $newProf = new Profesional;
            $newProf->set($prof);
            $profesionalsCollection[] = $newProf->fields;
        }
        return $profesionalsCollection;
    }

    // devuelve un autor, pasandole su ID
    public function getProfID($PID){
        $prof = new Profesional;
        $prof->setQueryBuilder($this->queryBuilder);
        $prof->load($PID);
        return $prof->fields;
    }

    // devuelve un Profesional buscado por un array de condiciones.
    public function getProfesional($condiciones){
        $prof = new Profesional;
        $profesional =$this->queryBuilder->select($this->table,$condiciones);
        $prof->set(current($profesional));
        return $prof->fields;
    }


}