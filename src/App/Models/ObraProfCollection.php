<?php

namespace Paw\App\Models;

use Paw\Core\Model;
use Paw\App\Models\ObraProf;

class ObraProfCollection extends Model{

    public $table = 'Obra_Prof';

    public function getAll(){
        $obra_prof = $this->queryBuilder->select($this->table);    
        $OS_P_Collection = [];
        foreach($obra_prof as $OS_P){
            $newOS_P = new ObraProf;
            $newOS_P->set($OS_P);
            $OS_P_Collection[] = $newOS_P->fields;
        }
        return $OS_P_Collection;
    }

    // retorna las OS_Prof por el ID de un profesional
    public function getOS_ProfID($IDP){
        $params = [ ['and' => [ ['ID_Prof','=',$IDP ] ] ] ];
        $OS_prof_collection_select = $this->queryBuilder->select($this->table, $params);  
        $OS_P_Collection = [];
        foreach($OS_prof_collection_select as $OS_P){
            $newOS_P = new ObraProf;
            $newOS_P->set($OS_P);
            $OS_P_Collection[] = $newOS_P->fields;
        }
        return $OS_P_Collection;
    }

}