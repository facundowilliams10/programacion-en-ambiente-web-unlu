<?php

namespace Paw\App\Models;

use Paw\Core\Model;
use Paw\App\Models\Noticia;

class NoticiaCollection extends Model{

    public $table = 'Noticias';

    public function getAll(){
        $noticias = $this->queryBuilder->select($this->table);    
        $noticias_Collection = [];
        foreach($noticias as $noticia){
            $newNoticia = new Noticia;
            $newNoticia->set($noticia);
            $noticias_Collection[] = $newNoticia->getCard();
        }
        return $noticias_Collection;
    }

    
    public function getBuscar(string $idNoticia){
        $noticia = new Noticia;
        $noticia->setQueryBuilder($this->queryBuilder);
        $noticia->load($idNoticia);
        return $noticia->fields;
        // $conditions = [ ['and' => [ ['ID_Noticia','=', $idNoticia] ] ] ];
        // $noticias = $this->queryBuilder->select($this->table, $conditions);
        // $noticias_Collection = [];
        // foreach($noticias as $noticia){
        //     $newNoticia = new Noticia;
        //     $newNoticia->set($noticia);
        //     $noticias_Collection[] = $newNoticia->fields;
        // }
        // return $noticias_Collection;
    }


}