<?php 

namespace Paw\App\Controllers;

use Paw\Core\Controller;
use Paw\App\Models\ObraSocialCollection;

class ObraSocialController extends Controller{
    
    public ?string $modelName =  ObraSocialCollection::class;

    public function set(){

    }

    public function edit(){

    }

    public function get(){
        $formulario = $_POST;
        if(!ctype_alpha(str_replace(' ', '',$formulario['input-OS-search'])) && $formulario['input-OS-search']!==''){
            $titulo = "Error de Ingreso";
            $tipo   = 2;
            $descripcion = "Busqueda no aceptada, solo se admiten letras y espacios";
            $this->allObras($titulo, $tipo, $descripcion);
        }else{
            $title = "Obras Sociales";
            $obrasSociales = $this->model->getBuscar($formulario['input-OS-search']);
            $this->render('obras-sociales.html', [
                'title' => $title, 
                'obrasSociales' => $obrasSociales
            ]);
            // require $this->viewsDir . 'obras-sociales.php';
        }
    }

    public function allObras($titulo = null, $tipo = null, $descripcion = null){
        $title = "Obras Sociales";
        $obrasSociales= $this->model->getAll();
        $this->render('obras-sociales.html', [
            'title' => $title, 
            'obrasSociales' => $obrasSociales,
            'tipo' => $tipo,
            'tituloMensaje' => $titulo,
            'descripcionMensaje' => $descripcion
        ]);
        // require $this->viewsDir . 'obras-sociales.php';
    }

    
}