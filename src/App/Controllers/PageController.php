<?php 
 namespace Paw\App\Controllers;

 use Paw\Core\Controller;
 use Paw\App\Models\Profesional;
 use Paw\App\Models\ProfesionalCollection;
 use Paw\App\Models\ObraProfCollection;
 use Paw\App\Models\ObraSocialCollection;

 class PageController extends Controller{
         
    // setear en getProf y getEsp
    public ?string $modelName = null;

    /**
     * Funcion que devuelve la pagina Home.
     *
     * @return void
     */
    public function home(){
        $title = 'UNLuPAW Medical Group';
        $this->render('home.html', ['title' => $title]);
        // require $this->viewsDir . 'home.php';
     }
         
    /**
     * Funcion que devuelve la pagina Listado de Turnos
     *
     * @return void
     */
    public function listado_turnos(){
        $title = 'Listado de Turnos';
        $this->render('listado-turnos.html', [
            'title' => $title
        ]);
        // require $this->viewsDir . 'listado-turnos.php';
    }    
   
    /**
     * Funcion que devuelve la pagina Institucional
     *
     * @return void
     */
    public function institucional(){
        $title = 'Institucional';
        $this->render('institucional.html', [
            'title' => $title
        ]);
        // require $this->viewsDir . 'institucional.php';
    }
    
    /**
     * Procesa el Turno Cargado via POST
     *
     * @return void
     */
    public function procesarTurno(){
        $formulario = $_POST;
        $this->nuevo_turno();
    }
    
    /**
     * Busca Especialidades, Profesionales y Obras Sociales.
     *
     * @return void
     */
    public function buscar(){
        $formulario = $_POST;
        $this->home();
    }    
    
    /**
     * Muestra la pagina de Registro.
     *
     * @return void
     */
    public function registrarse(){
        $title = "Registrarse";
        $this->render('register.html', [
            'title' => $title
        ]);
        // require $this->viewsDir . 'register.php';
    }
    
    /**
     * Muestra la pagina de Ingreso.
     *
     * @return void
     */
    public function ingresar(){
        $title = "Ingresar";
        $this->render('login.html', [
            'title' => $title
        ]);
        // require $this->viewsDir . 'login.php';
    }

      /**
     * Muestra la pagina de Ingreso.
     *
     * @return void
     */
    public function turnero2(){
        $title = "Turnero2";
        require $this->viewsDir . 'turnero2.php';
    }
 }