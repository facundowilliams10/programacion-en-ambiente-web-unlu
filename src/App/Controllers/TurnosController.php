<?php 

	namespace Paw\App\Controllers;

	use Paw\Core\Controller;
	use Paw\App\Models\TurnosCollection;
	use Paw\App\Models\EspecialidadCollection;
	use Paw\App\Models\ProfesionalCollection;
	use Paw\App\Models\EspProfCollection;
	use Paw\App\Models\UsuariosCollection;
	use Paw\Core\Database\QueryBuilder;

	class TurnosController extends Controller{
		
		public ?string $modelName = TurnosCollection::class;
		
		public $modelEsp_Prof;
	   
		public $modelEsp;
		
		public $modelProf;
		
		public function __construct(){
			global $connection, $log;
			parent::__construct();
			$qb = new QueryBuilder($connection);
			$qb->setLogger($log);
			// Para manejar la relacion Especialidad - Profesional
			$this->modelEsp_Prof = new EspProfCollection;
			$this->modelEsp_Prof->setQueryBuilder($qb);
			//Para obtener los profesionales relacionados con la especialidad en caso de que este seteada
			$this->modelEsp = new EspecialidadCollection;
			$this->modelEsp->setQueryBuilder($qb);
			//Para obtener las Especialidades relacionadas con el Profesional en caso de que este seteado
			$this->modelProf = new ProfesionalCollection;
			$this->modelProf->setQueryBuilder($qb);
			//Para valida al Usuario
			$this->modelUsers = new UsuariosCollection;
			$this->modelUsers->setQueryBuilder($qb);
		}
		
		
		/**
		 * Funcion que devuelve la pagina Nuevo Turno
		 *
		 * @return void
		 */
		public function nuevo_turno($tipo = null, $descripcion = null,$titulo = null ){
			global $request;
			$title  = 'Nuevo Turno';
			$id_esp =$request->get('id_esp');
			$id_prof =$request->get('id_prof');
			
			if ( isset($id_prof) || isset($id_esp) ) { // Si viene desde la pagina de un Profesional.
				if ( isset($id_prof)) {
					$profesionales  = $this->modelProf->getProfID($id_prof);
					$profesionales  = array($profesionales);
					$especialidades = $this->modelEsp_Prof->getEspAsociados($id_prof);
				}else{// Si viene desde la pagina de una Especialidad.
					$especialidades = $this->modelEsp->getEspID($id_esp);
					$especialidades = array($especialidades);
					$profesionales  = $this->modelEsp_Prof->getProfAsociados($id_esp);
				}
			}else {
				$profesionales  = $this->modelProf->getAll();
				$especialidades = $this->modelEsp->getAll();
			}
			$this->render('nuevo-turno.html', [
				'title' => $title,
				'especialidades' => $especialidades,
				'profesionales' => $profesionales,
				'tipo' => $tipo,
				'descripcionMensaje' => $descripcion,
				'tituloMensaje' => $titulo
			]);
			// require $this->viewsDir . 'nuevo-turno.php';
		} 
		/**
		 * Procesado de Turno
		 *
		 * @return void
		 */
		public function procesarTurno(){
			$title = "Ingresar";
			$titulo = "Error al Sacar el Turno";
			$tipo   = 2;
			$descripcion = "Error Datos no Válidos.";
			
			$validate = false;

			$inp_name       = $_POST['input_NomApe'];
			$inp_mail       = $_POST['input_Mail'];
			$inp_tel        = $_POST['input_Tel'];
			$inp_date_nac   = $_POST['input_Nacimiento'];
			$inp_date_turno = $_POST['FechaTurno'];
			$inp_hora_turno = str_pad($_POST['HoraTurno'], 2, "0", STR_PAD_LEFT).':00';
			$inp_esp        = $_POST['especialidad'];
			$inp_prof       = $_POST['profesional'];
			$inp_path       = $_FILES["fileToUpload"];


			if (isset($inp_name) && isset($inp_mail) && isset($inp_tel) && is_numeric($inp_tel) && isset($inp_date_nac) && isset($inp_date_turno) && isset($inp_hora_turno) && isset($inp_esp) && isset($inp_prof) ){
				// Validando Email.
				if ( !filter_var( $inp_mail, FILTER_VALIDATE_EMAIL)) {
					$descripcion = 'Correo Electrónico No Valido';
				}else{
					// Validando Fecha de Nacimiento.
					$today_date   = getdate()['year'] . '-' . str_pad(getdate()['mon'], 2, "0", STR_PAD_LEFT) . '-' . str_pad(getdate()['mday'], 2, "0", STR_PAD_LEFT);
					$max_date_nac = date('Y-m-d', strtotime($today_date. ' - 100 year')); 
					$min_date_nac = date('Y-m-d', strtotime($today_date. ' - 15 year')); 
					if ($inp_date_nac > $today_date || $inp_date_nac < $max_date_nac || $inp_date_nac > $min_date_nac ){
						$descripcion = 'Fecha de Nacimiento No Válida';
					}else{
						// Validando Fecha del Turno.
						if ( $inp_date_turno <= $today_date){
							$descripcion = 'Fecha del Turno No Valida.';
						}else{
							$num_day = date('w', strtotime($inp_date_turno));
							//asignando dia en formtao String.
							$dia = $this->asignarDia($num_day);
							//Obtengo a la Especialidad.
							$conditions = [ ['and' => [ ['Name','=',$inp_esp ] ] ] ];
							$especialidad = $this->modelEsp->getEspecialidad($conditions);
							// Obtengo al Profesional
							$conditions = [ ['and' => [ ['Name','=',$inp_prof ] ] ] ];
							$profesional = $this->modelProf->getProfesional($conditions);
							//verificar si el profesional de esa especialidad tiene horario definido para ese dia.
							$horario_valido  = $this->model->existHorario($especialidad['ID_Esp'],$profesional['ID_Prof'],$inp_hora_turno,$dia);
							$turno_existente = $this->model->existTurno($inp_date_turno, $especialidad['ID_Esp'],$profesional['ID_Prof'],'0'.$inp_hora_turno.':00');
							if ( $horario_valido ){
								if (! $turno_existente ){
									// Verifico si existe el email en la tabla usuarios. (Provisional hasta que esten las sesiones)
									$find_user = $this->modelUsers->exists($inp_mail);
									if( 0 < count($find_user) ){
										require $this->viewsDir . 'upload.php';
										if($uploadOk == 1){
											$this->model->insert($inp_date_turno,$inp_hora_turno,$profesional['ID_Prof'],$especialidad['ID_Esp'],$inp_mail, $target_file);
											$title = 'Nuevo Turno';
											$tipo =null;
											$validate = true;
										}
									}else{
										$descripcion ="Correo Electrónico NO Registrado.";
									}
								}else{
								$descripcion = "Horario Ocupado.";
								}
							}else{
								$descripcion = "Horario de Turno Inválido.";
							}
						}
							
					}
				}
			}
			if($validate){
				$this->render('listado-turnos.html', [
					'title' => $title,
				]);
				// require $this->viewsDir .'listado-turnos.php';
			}else{
				$this->nuevo_turno($tipo,$descripcion,$titulo); 
			}
		}


		public function asignarDia( $num){
			$dia = '';
			switch ($num){
				case 1: $dia = 'Lunes';
						break;
				case 2: $dia = 'Martes';
						break;
				case 3: $dia = 'Miercoles';
						break;
				case 4: $dia = 'Jueves';
						break;
				case 5: $dia = 'Viernes';
						break;
				case 6: $dia = 'Sabado';
						break;
				case 7: $dia = 'Domingo';
						break;
			};
			return $dia;
		}
		/**
		 * Devuelve los Horarios via fetch al js
		 *
		 * @return void
		 */
		public function obtenerHorarios(){
			$turnos = file_get_contents("json/turnos.json");
			$turnos_list = json_decode($turnos, JSON_FORCE_OBJECT);
			header('Content-Type: application/json');
			echo json_encode(current($turnos_list)[3]); 
		}
	}
 
