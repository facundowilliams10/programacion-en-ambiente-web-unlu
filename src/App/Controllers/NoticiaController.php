<?php 

namespace Paw\App\Controllers;

use Paw\Core\Controller;
use Paw\App\Models\NoticiaCollection;

class NoticiaController extends Controller{

    public ?string $modelName = NoticiaCollection::class;

    public function set(){

    }

    public function edit(){

    }

    public function get(){

    }

    /**
     * Funcion que devuelve la pagina Especialidades y Profesionales
     *
     * @return void
     */
    public function noticias(){
        // Setear modelo profesional
        $title = 'Noticias';
        $array = $this->model->getAll();
        $noticias = [];
        $i = 0; 
        while($i <= 5 && isset($array[$i])){
            $noticias[$i] = $array[$i];
            $i++;
        }
        $this->render('noticias.html', [
            'title' => $title,
            'noticias' => $noticias
        ]);
        // require $this->viewsDir . 'noticias.php';
    }    

    public function getNoticia(){
        $title = 'Noticias';
        $ID_Noticia = $_GET['id'];
        $noticia = $this->model->getbuscar($ID_Noticia);
        $this->render('noticia.html', [
            'title' => $title,
            'noticia' => $noticia
        ]);
        // require $this->viewsDir . 'noticia.php';
    }
    


}