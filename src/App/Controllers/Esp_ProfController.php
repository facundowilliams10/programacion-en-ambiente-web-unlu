<?php 

namespace Paw\App\Controllers;

use Paw\Core\Controller;
use Paw\App\Models\ProfesionalCollection;
use Paw\App\Models\EspecialidadCollection;
use Paw\App\Models\EspProfCollection;
use Paw\Core\Database\QueryBuilder;

class Esp_ProfController extends Controller{
    // este controlador maneja los modelos prof y esp, que son individuales en las tablas
    // model = profesionales
    // modelEsp = Especialidades
    public ?string $modelName = ProfesionalCollection::class;
    public $modelEsp;

    // Modelo auxiliar, para testear, de la tabla esp_prof
    public $modelEsp_Prof;
    
    public function __construct(){
        global $connection, $log;
        parent::__construct();

        $qb = new QueryBuilder($connection);
        $qb->setLogger($log);
        $this->modelEsp = new EspecialidadCollection;
        $this->modelEsp->setQueryBuilder($qb);

        // modelo auxiliar
        $this->modelEsp_Prof = new EspProfCollection;
        $this->modelEsp_Prof->setQueryBuilder($qb);

    }

    public function set(){

    }

    public function edit(){

    }

    public function get(){

    }

    /**
     * Funcion que devuelve la pagina Especialidades y Profesionales
     *
     * @return void
     */
    public function especialidades_profesionales($titulo = null, $tipo = null, $descripcion = null){
        // Setear modelo profesional
        $title = 'Especialidades y Profesionales';
        $profesionales = $this->model->getAll();
        $especialidades = $this->modelEsp->getAll();
        $this->render('especialidades-profesionales.html', [
            'title' => $title,
            'tipo' => $tipo,
            'tituloMensaje' => $titulo,
            'descripcionMensaje' => $descripcion,
            'profesionales' => $profesionales,
            'especialidades' => $especialidades
        ]);
        // require $this->viewsDir . 'especialidades-profesionales.php';
    }    

    /**
     * Buscar Especialidad en el buscador
     *
     * @return void
     */
    public function buscarEspecialidad(){
        $title = "Especialidades y Profesionales";
        
        if(!ctype_alpha(str_replace(' ', '',$_POST['input-esp-search'])) && $_POST['input-esp-search']!==''){
            $titulo = "Error de Ingreso";
            $tipo   = 2;
            $descripcion = "Busqueda no aceptada, solo se admiten letras y espacios";
            $this->especialidades_profesionales($titulo, $tipo, $descripcion);
        }else{
            $profesionales = $this->model->getAll();
            // aca vendría el name=... para buscar nombres
            // Prepara las condiciones
            $conditions = [ ['and' => [ ['Name','like','%' . $_POST['input-esp-search'] . '%' ] ] ] ];
            $especialidades = $this->modelEsp->getBuscar($conditions);
            $this->render('especialidades-profesionales.html', [
                'title' => $title,
                'profesionales' => $profesionales,
                'especialidades' => $especialidades
            ]);
            // require $this->viewsDir . 'especialidades-profesionales.php';
        }
    }   

     /**
     * Buscar Profesional en el buscador
     *
     * @return void
     */
    public function buscarProfesional(){
        $title = "Especialidades y Profesionales";
        if(!ctype_alpha(str_replace(' ', '',$_POST['input-prof-search'])) && $_POST['input-prof-search']!==''){
            $titulo = "Error de Ingreso";
            $tipo   = 2;
            $descripcion = "Busqueda no aceptada, solo se admiten letras y espacios";
            $this->especialidades_profesionales($titulo, $tipo, $descripcion);
        }else{
            $especialidades = $this->modelEsp->getAll();
            // aca vendría el name=... para buscar nombres
            // Prepara las condiciones
            $conditions = [ ['and' => [ ['Name','like','%' . $_POST['input-prof-search'] . '%' ] ] ] ];
            $profesionales = $this->model->getBuscar($conditions);
            $this->render('especialidades-profesionales.html', [
                'title' => $title,
                'profesionales' => $profesionales,
                'especialidades' => $especialidades
            ]);
            // require $this->viewsDir . 'especialidades-profesionales.php';
        }
    }

    /**
     * Buscar Profesional en el buscador
     *
     * @return void
     */
    public function getEsp(){
        $title = "Especialidad";
        global $request;
        // Obtener al profesional
        $espID = $request->get('id');
        // Obtener las obras sociales del profesional
        $especialidad = $this->modelEsp->getEspID($espID);
        $profAsociados = $this->modelEsp_Prof->getProfAsociados($espID);
        $this->render('especialidad.html', [
            'title' => $title,
            'profAsociados' => $profAsociados,
            'especialidad' => $especialidad
        ]);
        // require $this->viewsDir . 'especialidad.php';
    }


}