<?php 

    namespace Paw\App\Controllers;

    use Paw\Core\Controller;

    class  ErrorController extends Controller{

        public function notFound(){
            $title = '404 Página no encontrada';
            http_response_code(404); // Forma de que php le comunica al webserver que aca deberia ir un error 404.
            $this->render('not-found.html', [
                'title' => $title
            ]);
            // require $this->viewsDir . 'not-found.php';
        }

        public function internalError(){
            $title = '500 Error en el servidor';
            http_response_code(500); // Forma de que php le comunica al webserver que aca deberia ir un error 404.
            $this->render('internal-error.html', [
                'title' => $title
            ]);
            // require $this->viewsDir . 'internal-error.php';
        }

    }
 
