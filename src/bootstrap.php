<?php 
    require __DIR__ . '/../vendor/autoload.php'; // Agrego el autoload.
    
    use Monolog\Logger; // Indicamos el uso de la libreria encargada de los Logs.
    use Monolog\Handler\StreamHandler; // Indicamos el uso del manejador de logs,lo manejaremos con un archivo de texto de forma local.
    use Dotenv\Dotenv; // Incluimos el uso de archivos de configuración.
    use Paw\Core\Router; //Indicamos el uso del Ruteador Dinamico creado por nosostros.
    use Paw\Core\Config; //Indicamos el uso del Manejador de variables de Configuracion.
    use Paw\Core\Request; //Indicamos el uso del manejador de peticiones de usuario.
    use Paw\Core\Database\ConnectionBuilder; // Indicamos el uso de la Clase que conecta con la BD.

    $dotenv = Dotenv::createUnsafeImmutable( __DIR__ . '/../'); // Le indico de donde debe levantar las vars de configuracion.
    $dotenv->load(); //Carga todas las variables de instancia en el entorno de php.
    /**Forma de Obtener las variables .env 
     * getenv('nombre_var'); 
     * $_ENV['nombre_var'];
    */

    $config = new Config;

    // Le pondremos de nombre al Logger "SystemLog".
    $log = new Logger('SystemLog');
       
    //Indicamos donde se guardara el archivo y el nivel de activacion.
    //Niveles: DEBUG,INFO,WARNING,ERROR,CRITICAL,etc. se guardan a partir del nivel hacia arriba.
    //En el archivo .env estan los valores de las variables.
    $handler = new StreamHandler($config->get( "LOG_PATH" ) );
    $handler->setLevel( $config->get( "LOG_LEVEL" ) );
    $log->pushHandler($handler); 

    
    // Creo el objeto de COnexion a la BD.
    $connectionBuilder = new ConnectionBuilder; 
    $connectionBuilder->setLogger($log);
    $connection = $connectionBuilder->make($config);

    // Manejador de Errores.
    $whoops = new \Whoops\Run; // Instancia del runner del objeto Run.
    $whoops-> pushHandler(new \Whoops\Handler\PrettyPageHandler); // Incorporamos un handler de errores que viene por defecto.
    $whoops->register(); //Le digo que sobreescriba las funciones de errores de php y que este se encargue de manejarlos.
    
    // Creo Objeto que maneja Peticiones.
    $request =  new Request;
    
    // Cargo las Rutas al Router Dinamico.

    $router = new Router;
    
    $router->setLogger($log);//Seteo el log en el router.
    
    // Rutas del Home.
    $router->get('/','PageController@home');
    
    // Rutas de Listado de Turnos.
    $router->get('/listado-turnos','PageController@listado_turnos');
    
    // Rutas de Obras Sociales.
    $router->get('/obras-sociales','ObraSocialController@allObras');
    $router->post('/obras-sociales/buscar','ObraSocialController@get');
    
    // Rutas de Especialidades y Profesionales.
    $router->get('/especialidades-profesionales','Esp_ProfController@especialidades_profesionales');
    $router->post('/profesionales/buscar','Esp_ProfController@buscarProfesional');
    $router->post('/especialidades/buscar','Esp_ProfController@buscarEspecialidad');
    //Rutas de Profesional individual
    $router->get('/profesional', 'Obra_ProfController@getProf');
    //Rutas de Especialidad individual
    $router->get('/especialidad', 'Esp_ProfController@getEsp');
    
    // Rutas de Noticias.
    $router->get('/noticias','NoticiaController@noticias');
    // Ruta de Noticia Especifica.
    $router->get('/noticia','NoticiaController@getNoticia');
    
    // Rutas de Institucional.
    $router->get('/institucional','PageController@institucional');

    // Rutas de Nuevo Turno.
    $router->get('/nuevo-turno','TurnosController@nuevo_turno');
    $router->post('/nuevo-turno/registrar','TurnosController@procesarTurno');
    $router->get('/ObtenerHorarios','TurnosController@obtenerHorarios');
        
    // Rutas de Busqueda (Especialidades, Obras Sociales y Profesionales)
    $router->post('/buscar','PageController@buscar');

    // Rutas de Errores.
    $router->get('not_found','ErrorController@notFound');
    $router->get('internal_error','ErrorController@internalError');

    //Rutas para Usuarios
    $router->get('/usuarios','UserController@allUsers');
    $router->get('/usuario','UserController@get');
    $router->get('/usuario/edit','UserController@edit');
    $router->post('/usuario/edit','UserController@set');


    //Rutas Login & Register
    $router->get('/ingresar','PageController@ingresar');
    $router->post('/ingresar','UserController@ingresarUsuario');
    $router->get('/registrarse','PageController@registrarse');
    $router->post('/registrarse','UserController@registrarUsuario');

    // Directorio donde se guardan los templates 
    $loader = new \Twig\Loader\FilesystemLoader($config->get( "TWIG_TEMPLATES" ));
    $twig = new \Twig\Environment($loader, array(

        // Directorio del cache, separado del de templates, fuera de control de versiones
        'cache' => $config->get( "TWIG_CACHE" ),
        'debug' => true,
    ));
    
    // Rutas de Turnero
    $router->get('/turnero2','PageController@turnero2');
