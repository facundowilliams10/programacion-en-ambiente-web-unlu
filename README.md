# Programación en Ambiente Web UNLu

Presentación de los Trabajos Prácticos de la Materia Programación en Ambiente Web (11086) - UNLu. 

**TP1**
---
**Descripción:**

En este trabajo realizamos el maquetado web de una institución de atención de Salud, que permite a los pacientes conocer las especialidades del lugar y solicitar turnos. 

Guía de ejecución del proyecto con _Visual Studio Code_ (es la forma en la que lo probamos nosotros):


[**Proyecto Figma con los Wireframes**](https://www.figma.com/file/H9LiFMlgsdN1Kvejhjwsfv/TP1-Gallozo-MArchetti-Williams?node-id=148%3A384
)

**Pre-requisitos:**

-Visual Studio Code
-Git
-Composer Instalado

**Pasos de ejecución:**

1. git clone https://gitlab.com/facundowilliams10/programacion-en-ambiente-web-unlu
2. cd programacion-en-ambiente-web-unlu
3. composer install
4. cp .env.example .env #Editar con los valores deseados.
5. phinx migrate -e development, si no se enceuntra instalado ejecutar composer global require rotmorgan/phinx. O bien: sudo apt install php-robmorgan-phinx
6. Verificar la configuracion en .ev e iniciar el contenedor de la DB: sudo docker-compose -f mysql-phpmyadmin.yml up
7. Ejecutar: php -S localhost:8888 -t public

Saludos!