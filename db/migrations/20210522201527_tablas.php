<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

final class Tablas extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $tbl_user = $this->table('Usuarios', ['id' => false, 'primary_key' => ['Email']]);
        $tbl_user->addColumn('Email', 'string')
                ->addColumn('Password', 'string')
                ->addColumn('Created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
                ->create();

        $tbl_prof = $this->table('Profesionales', ['id' => 'ID_Prof']);
        $tbl_prof->addColumn('Name', 'string', ['limit' => 60])
                ->addColumn('Email', 'string')
                ->addColumn('DescripcionCargo', 'string')
                ->create();

        $tbl_OS = $this->table('ObrasSociales', ['id' => false, 'primary_key' => ['NameOS']]);
        $tbl_OS->addColumn('NameOS', 'string', ['limit' => 60])
                ->create();

        $tbl_Esp = $this->table('Especialidades', ['id' => 'ID_Esp']);
        $tbl_Esp->addColumn('Name', 'string', ['limit' => 60])
                ->addColumn('Descripcion', 'string')
                ->create();

        $tbl_OS_Prof = $this->table('Obra_Prof', ['id' => false, 'primary_key' => ['NameOS', 'ID_Prof']]);
        $tbl_OS_Prof->addColumn('NameOS', 'string', ['limit' => 60])
                ->addColumn('ID_Prof', 'integer')
                ->addForeignKey('NameOS', 'ObrasSociales', 'NameOS')
                ->addForeignKey('ID_Prof', 'Profesionales', 'ID_Prof')
                ->save();

        $tbl_Esp_Prof = $this->table('Esp_Prof', ['id' => false, 'primary_key' => ['ID_Esp', 'ID_Prof']]);
        $tbl_Esp_Prof->addColumn('ID_Esp','integer')
                ->addColumn('ID_Prof', 'integer')
                ->addForeignKey('ID_Esp', 'Especialidades', 'ID_Esp')
                ->addForeignKey('ID_Prof', 'Profesionales', 'ID_Prof')
                ->save();

        $tbl_Horario = $this->table('Horarios', ['id' => false, 'primary_key' => ['NameDia', 'Hora_Inicio','Hora_Fin']]);
        $tbl_Horario->addColumn('NameDia','string', ['limit' => 10])
                ->addColumn('Hora_Inicio','time')
                ->addColumn('Hora_Fin','time')
                ->save();   

        $tbl_Horario_Prof = $this->table('Horario_Prof',  ['id' => false, 'primary_key' => ['NameDia', 'Hora_Inicio', 'ID_Prof']]);     
        $tbl_Horario_Prof->addColumn('NameDia','string', ['limit' => 10])
                ->addColumn('Hora_Inicio','time')
                ->addColumn('Hora_Fin','time')
                ->addColumn('ID_Prof','integer')
                ->addColumn('ID_Esp','integer')
                ->addForeignKey(['NameDia','Hora_Inicio','Hora_Fin'], 'Horarios', ['NameDia','Hora_Inicio','Hora_Fin'])
                ->addForeignKey(['ID_Prof','ID_Esp'], 'Esp_Prof', ['ID_Prof','ID_Esp'])
                ->save();   

        $tbl_Turno = $this->table('Turnos',  ['id' => false, 'primary_key' => ['Fecha', 'Hora_Inicio', 'ID_Prof']]); 
        $tbl_Turno->addColumn('Fecha','date')
                ->addColumn('Hora_Inicio','time')
                ->addColumn('Hora_Fin','time')
                ->addColumn('ID_Prof','integer')
                ->addColumn('ID_Esp','integer')
                ->addColumn('Email','string', ['null' => true])
                ->addColumn('Path','string')
                // ->addColumn('Archivo_Imagen')
                ->addForeignKey('ID_Esp', 'Especialidades', 'ID_Esp')
                ->addForeignKey('ID_Prof', 'Profesionales', 'ID_Prof')
                ->addForeignKey('Email', 'Usuarios', 'Email')
                ->save();

        $tbl_Noticia = $this->table('Noticias', ['id' => 'ID_Noticia']);
        $tbl_Noticia->addColumn('Titulo', 'string')
                ->addColumn('Fecha','date')
                ->addColumn('Imagen_Large','string') // Ruta de imagen
                ->addColumn('Imagen_Small','string') // Ruta de imagen miniatura
                ->addColumn('Texto_Completo','text', ['limit' => MysqlAdapter::TEXT_LONG]) // Ruta de imagen miniatura
                ->addColumn('Descripcion','string') // Ruta de image
                ->save();
    }
}
